<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>${client.fullName} - OSKManager</title>
    <%@include file="static/heads.jsp" %>
</head>

<body>
<!-- preloader area start -->
<div id="preloader">
    <div class="loader"></div>
</div>
<!-- preloader area end -->
<!-- page container area start -->
<div class="page-container">
    <%@include file="static/menu.jsp" %>
    <!-- main content area start -->
    <div class="main-content">
        <!-- header area start -->
        <div class="header-area">
            <div class="row align-items-center">
                <!-- nav and search button -->
                <div class="col-md-6 col-sm-8 clearfix">
                    <div class="nav-btn pull-left">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <!-- profile info & task notification -->
            </div>
        </div>
        <!-- header area end -->
        <!-- page title area start -->
        <div class="page-title-area">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="breadcrumbs-area clearfix">
                        <h4 class="page-title pull-left">${client.fullName}</h4>
                        <ul class="breadcrumbs pull-left">
                            <li><a href="${pageContext.request.contextPath}/dashboard">Ewidencja</a></li>
                            <li><a href="${pageContext.request.contextPath}/client/list">Kursanci</a></li>
                            <li>${client.fullName}</li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 clearfix">
                    <div class="user-profile pull-right">
                        <%@ page import="pl.kmo2008.oskmanager.entity.enums.Sex" %>
                        <c:choose>
                            <c:when test="${user.sex == Sex.MAN}">
                                <img class="avatar user-thumb"
                                     src="${pageContext.request.contextPath}/static/images/author/avatar_man.png"
                                     alt="avatar">
                            </c:when>
                            <c:otherwise>
                                <img class="avatar user-thumb"
                                     src="${pageContext.request.contextPath}/static/images/author/avatar_woman.png"
                                     alt="avatar">
                            </c:otherwise>
                        </c:choose>
                        <h4 class="user-name">${user.fullName}</h4>
                    </div>
                </div>
            </div>
        </div>
        <!-- page title area end -->
        <div class="main-content-inner">
            <section>
                <div class="mt-5">
                    <div class="card">
                        <div class="card-body">
                            <div class="container-fluid">
                                <form action="${pageContext.request.contextPath}/client/edit/${client.id}"
                                      method="post">
                                    <input type="hidden" name="name" value="${client.name}">
                                    <input type="hidden" name="secondName" value="${client.secondName}">
                                    <input type="hidden" name="lastName" value="${client.lastName}">
                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                    <div class="row">
                                        <div class="col-auto ml-auto">
                                            <button type="submit" class="btn btn-success">Zapisz</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <%--3 row--%>
                                        <div class="col-4 mb-3">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control input-rounded" id="email"
                                                   value="${client.email}"
                                                   name="email"
                                                   required="">
                                            <div class="invalid-feedback">
                                                Niepoprawny adres email.
                                            </div>
                                        </div>
                                        <div class="col-4 mb-3">
                                            <label for="pesel">Pesel</label>
                                            <input type="text" class="form-control input-rounded" id="pesel"
                                                   name="pesel"
                                                   value="${client.pesel}"
                                                   pattern="[0-9]{11}" required>
                                            <div class="invalid-feedback">
                                                Niepoprawny PESEL.
                                            </div>
                                        </div>
                                        <div class="col-4 mb-3">
                                            <label>Płeć</label>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" checked id="sexM" name="sex"
                                                       class="custom-control-input">
                                                <label class="custom-control-label" for="sexM">Mężczyzna</label>
                                            </div>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="sexW" name="sex" class="custom-control-input">
                                                <label class="custom-control-label" for="sexW">Kobieta</label>
                                            </div>
                                        </div>
                                    </div>
                                    <%--4 row--%>
                                    <div class="row">
                                        <div class="col-4 mb-3">
                                            <label for="street">Ulica</label>
                                            <input type="text" class="form-control input-rounded" id="street"
                                                   name="street"
                                                   value="${client.street}"
                                                   required>
                                            <div class="valid-feedback">
                                                Ok!
                                            </div>
                                        </div>
                                        <div class="col-4 mb-3">
                                            <label for="houseNumber">Numer domu</label>
                                            <input type="text" class="form-control input-rounded" id="houseNumber"
                                                   value="${client.houseNumber}"
                                                   name="houseNumber" required>
                                            <div class="valid-feedback">
                                                Ok!
                                            </div>
                                        </div>
                                        <div class="col-4 mb-3">
                                            <label for="apartmentNumber">Numer mieszkania</label>
                                            <input type="text" class="form-control input-rounded" id="apartmentNumber"
                                                   value="${client.apartmentNumber}"
                                                   name="apartmentNumber">

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4 mb-2">
                                            <label for="document" class="col-form-label">Dokument</label>
                                            <select class="form-control input-rounded" style="height: 50px !important;"
                                                    id="document" name="document">
                                                <option value="ID">Dowód osobisty</option>
                                                <option value="PASSPORT">Paszport</option>
                                                <option value="OTHER">Inny</option>
                                            </select>
                                        </div>
                                        <div class="col-4 mb-3">
                                            <label for="documentNumber">Numer dokumentu</label>
                                            <input type="text" class="form-control input-rounded" id="documentNumber"
                                                   value="${client.documentNumber}"
                                                   name="documentNumber" required>
                                            <div class="valid-feedback">
                                                Ok!
                                            </div>
                                        </div>
                                        <div class="col-4 mb-3">
                                            <label for="phone">Telefon</label>
                                            <input type="text" class="form-control input-rounded" id="phone"
                                                   value="${client.phone}"
                                                   name="phone" required>
                                            <div class="valid-feedback">
                                                Ok!
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="row">
                                    <div class="col-lg-12 mt-5">
                                        <div class="card">
                                            <div class="card-body">
                                                <h4 class="header-title">Profil Kandydata na kierowcę</h4>
                                                <div class="single-table">
                                                    <div class="table-responsive">
                                                        <table class="table text-center" id="pkkTable">
                                                            <thead class="text-uppercase bg-dark">
                                                            <tr class="text-white">
                                                                <th scope="col">Data</th>
                                                                <th scope="col">Numer</th>
                                                                <th scope="col">Kategorie</th>
                                                                <th scope="col">Akcje &nbsp;
                                                                    <button type="button"
                                                                            class="btn btn-success btn-xs btn-rounded"
                                                                            data-toggle="modal"
                                                                            data-target=".modal-add-pkk">+
                                                                    </button>
                                                                    <div class="modal fade modal-add-pkk">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content text-left">
                                                                                <form action="${pageContext.request.contextPath}/pkk/add/${client.id}"
                                                                                      method="post">
                                                                                    <input type="hidden"
                                                                                           name="${_csrf.parameterName}"
                                                                                           value="${_csrf.token}"/>
                                                                                    <div class="modal-body">
                                                                                        <div class="form-row">
                                                                                            <div class="col-md-12 mb-3">
                                                                                                <label for="pkkNumber">Numer
                                                                                                    Pkk</label>
                                                                                                <input class="form-control input-rounded"
                                                                                                       type="text"
                                                                                                       id="pkkNumber"
                                                                                                       name="pkkNumber">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-row">
                                                                                            <div class="col-md-12 mb-3">
                                                                                                <b class="text-muted mb-3 mt-4 d-block">Kategorie</b>
                                                                                                <c:forEach
                                                                                                        items="${categories}"
                                                                                                        var="cat">
                                                                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                                                                        <input type="checkbox"
                                                                                                               class="custom-control-input"
                                                                                                               name="${cat.name}"
                                                                                                               id="${cat.name}">
                                                                                                        <label class="custom-control-label"
                                                                                                               for="${cat.name}">${cat.name}</label>
                                                                                                    </div>
                                                                                                </c:forEach>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="button"
                                                                                                class="btn btn-secondary"
                                                                                                data-dismiss="modal">
                                                                                            Zamknij
                                                                                        </button>
                                                                                        <button type="submit"
                                                                                                class="btn btn-primary">
                                                                                            Zapisz
                                                                                        </button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <c:forEach var="pkk" items="${client.pkk}">
                                                                <tr class="table-secondary">
                                                                    <th scope="row">${pkk.addDate}</th>
                                                                    <td>${pkk.number}</td>
                                                                    <td>
                                                                        <c:forEach items="${pkk.categories}"
                                                                                   var="cat">
                                                                            ${cat.name},
                                                                        </c:forEach>
                                                                    </td>
                                                                    <td>
                                                                        <a href="${pageContext.request.contextPath}/pkk/delete/${pkk.id}/${client.id}">
                                                                            <button type="button"
                                                                                    class="btn btn-danger mb-3"><span
                                                                                    class="fa fa-trash"></span> Usuń PKK
                                                                            </button>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </c:forEach>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-auto ml-auto  mt-5">
                                        <button type="button" class="btn btn-danger mb-3" data-toggle="modal"
                                                data-target=".modal-delete-name"><span
                                                class="fa fa-trash"></span> Usuń kursanta
                                        </button>
                                        <div class="modal fade modal-delete-name">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        Czy napewno chcesz usunąć <b>${client.fullName}</b>?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">
                                                            Zamknij
                                                        </button>
                                                        <a href="${pageContext.request.contextPath}/client/delete/${client.id}"
                                                           class="btn btn-danger"><span
                                                                class="fa fa-trash"></span> Usuń</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>


    <!-- main content area end -->
    <!-- footer area start-->
    <footer>
        <div class="footer-area">
            <p>© Copyright 2018. All right reserved. Template by <a href="https://colorlib.com/wp/">Colorlib</a>.</p>
        </div>
    </footer>
    <!-- footer area end-->
</div>
<!-- page container area end -->
<%@include file="static/footer.jsp" %>

<div id="jsVariables"
     data-sex="${client.sex}"
     data-document="${client.document}"
>
</div>
</body>




<script type="application/javascript">

    let sex = $('#jsVariables').data('sex');
    let idocument = $('#jsVariables').data('document');
    if (sex === "MAN") {
        document.getElementById("sexM").checked = true;
    } else document.getElementById("sexW").checked = true;
    document.getElementById("document").value = idocument;

    $(document).ready(function () {
        $('#pkkTable').DataTable({
            "language": {
                "decimal": "",
                "emptyTable": "Brak danych w tabeli",
                "info": "Rekordy _START_ do _END_ z _TOTAL_",
                "infoEmpty": "Rekordy 0 do 0 z 0 ",
                "infoFiltered": "(filtered from _MAX_ total entries)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Pokaż _MENU_ rekordów",
                "loadingRecords": "Ładowanie...",
                "processing": "Przetwarzanie...",
                "search": "Wyszukaj:",
                "zeroRecords": "Nie znaleziono rekordów",
                "paginate": {
                    "first": "Pierwszy",
                    "last": "Ostatni",
                    "next": "Następny",
                    "previous": "Poprzedni"
                },
                "aria": {
                    "sortAscending": ": sortuj rosnąco",
                    "sortDescending": ": sortuj malejąco"
                }
            }
        });
    });

</script>

</html>