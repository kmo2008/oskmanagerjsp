<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Transakcje - OSKManager</title>
    <%@include file="static/heads.jsp" %>
</head>

<body>
<!-- preloader area start -->
<div id="preloader">
    <div class="loader"></div>
</div>
<!-- preloader area end -->
<!-- page container area start -->
<div class="page-container">
    <%@include file="static/menu.jsp" %>
    <!-- main content area start -->
    <div class="main-content">
        <!-- header area start -->
        <div class="header-area">
            <div class="row align-items-center">
                <!-- nav and search button -->
                <div class="col-md-6 col-sm-8 clearfix">
                    <div class="nav-btn pull-left">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <!-- profile info & task notification -->
            </div>
        </div>
        <!-- header area end -->
        <!-- page title area start -->
        <div class="page-title-area">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="breadcrumbs-area clearfix">
                        <h4 class="page-title pull-left">Strona Główna</h4>
                        <ul class="breadcrumbs pull-left">
                            <li><a href="${pageContext.request.contextPath}/dashboard">Strona Główna</a></li>
                            <li>Transakcje</li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 clearfix">
                    <div class="user-profile pull-right">
                        <%@ page import="pl.kmo2008.oskmanager.entity.enums.Sex" %>
                        <c:choose>
                            <c:when test="${user.sex == Sex.MAN}">
                                <img class="avatar user-thumb"
                                     src="${pageContext.request.contextPath}/static/images/author/avatar_man.png"
                                     alt="avatar">
                            </c:when>
                            <c:otherwise>
                                <img class="avatar user-thumb"
                                     src="${pageContext.request.contextPath}/static/images/author/avatar_woman.png"
                                     alt="avatar">
                            </c:otherwise>
                        </c:choose>
                        <h4 class="user-name">${user.fullName}</h4>
                    </div>
                </div>
            </div>
        </div>
        <!-- page title area end -->
        <div class="main-content-inner">
            <c:forEach items="${clientCourses}" var="course">
                <c:set var="sum" value="0"/>
                <c:forEach items="${clientTransactions}" var="transaction">
                    <c:if test="${transaction.course == course}">
                        <c:set var="sum" value="${sum + transaction.value}"/>
                    </c:if>
                </c:forEach>

                <c:set var="percent" value="${sum/course.price*100}"/>
                <section>

                    <div class="col-lg-12 mt-5">
                        <div class="card pl-3 pt-3">
                            <div class="card-title">
                                <h4>${course.courseNumber} (kat. ${course.category.name}) | ${course.price} zł</h4>
                                <div class="progress mb-5 mt-5" style="height: 25px;">
                                    <div class="progress-bar progress-bar-striped bg-success progress-bar-animated"
                                         role="progressbar" style="width: ${percent}%;" aria-valuenow="${sum}"
                                         aria-valuemin="0" aria-valuemax="${course.price}">${sum} zł / ${course.price} zł
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="col-md-4 mb-3">
                                    <div class="single-table">
                                        <div class="table-responsive-sm">
                                            <table class="table text-center">
                                                <thead class="text-uppercase bg-secondary">
                                                <tr class="text-white">
                                                    <th scope="col">Data</th>
                                                    <th scope="col">Wartość</th>
                                                    <th scope="col">Akcja</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:forEach items="${clientTransactions}" var="transaction">
                                                    <c:if test="${transaction.course == course}">
                                                        <tr>
                                                            <th scope="row">${transaction.date}</th>
                                                            <td>${transaction.value} zł</td>
                                                            <td><a href="/transaction/delete/${transaction.id}"><button class="btn btn-danger"><i class="fa fa-trash"></i></button></a> </td>
                                                        </tr>
                                                    </c:if>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <c:if test="${percent < 100}">
                                    <div class="col-md-4 mb-3">
                                        <form action="${pageContext.request.contextPath}/transaction/add" method="post">
                                            <input type="hidden" name="clientId" value="${client.id}">
                                            <input type="hidden" name="courseId" value="${course.id}">
                                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                            <div class="form-row">
                                                <label class="col-form-label" for="payment">Kwota</label>
                                                <div class="input-group">
                                                    <input type="number" class="form-control input-rounded" id="payment"
                                                           max="${course.price - sum}"
                                                           name="payment" required>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">zł</span>
                                                    </div>
                                                    <div class="valid-feedback">
                                                        Ok!
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn btn-success mt-2">Zapisz</button>
                                            </div>
                                        </form>
                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </section>
            </c:forEach>
        </div>
    </div>
    <!-- main content area end -->
    <!-- footer area start-->
    <footer>
        <div class="footer-area">
            <p>© Copyright 2018. All right reserved. Template by <a href="https://colorlib.com/wp/">Colorlib</a>.</p>
        </div>
    </footer>
    <!-- footer area end-->
</div>
<!-- page container area end -->
<%@include file="static/footer.jsp" %>
</body>

</html>