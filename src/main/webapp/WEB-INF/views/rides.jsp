<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Jazdy - OSKManager</title>
    <%@include file="static/heads.jsp" %>
</head>

<body>
<!-- preloader area start -->
<div id="preloader">
    <div class="loader"></div>
</div>
<!-- preloader area end -->
<!-- page container area start -->
<div class="page-container">
    <%@include file="static/menu.jsp" %>
    <!-- main content area start -->
    <div class="main-content">
        <!-- header area start -->
        <div class="header-area">
            <div class="row align-items-center">
                <!-- nav and search button -->
                <div class="col-md-6 col-sm-8 clearfix">
                    <div class="nav-btn pull-left">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <!-- profile info & task notification -->
            </div>
        </div>
        <!-- header area end -->
        <!-- page title area start -->
        <div class="page-title-area">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="breadcrumbs-area clearfix">
                        <h4 class="page-title pull-left">Strona Główna</h4>
                        <ul class="breadcrumbs pull-left">
                            <li><a href="${pageContext.request.contextPath}/dashboard">Strona główna</a></li>
                            <li>Jazdy</li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 clearfix">
                    <div class="user-profile pull-right">
                        <%@ page import="pl.kmo2008.oskmanager.entity.enums.Sex" %>
                        <c:choose>
                            <c:when test="${user.sex == Sex.MAN}">
                                <img class="avatar user-thumb"
                                     src="${pageContext.request.contextPath}/static/images/author/avatar_man.png"
                                     alt="avatar">
                            </c:when>
                            <c:otherwise>
                                <img class="avatar user-thumb"
                                     src="${pageContext.request.contextPath}/static/images/author/avatar_woman.png"
                                     alt="avatar">
                            </c:otherwise>
                        </c:choose>
                        <h4 class="user-name">${user.fullName}</h4>
                    </div>
                </div>
            </div>
        </div>
        <!-- page title area end -->
        <div class="main-content-inner">
            <section>
                <div class="col-lg-12 mt-5">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <div class="float-right mb-5">
                                    <c:if test="${instr == true}">
                                        <button type="button" class="btn btn-success mb-3">
                                                ${payment} zł
                                        </button>
                                        &nbsp;
                                    </c:if>
                                    <a href="${pageContext.request.contextPath}/ride/list/1/0/${today}">
                                        <button type="button" class="btn btn-primary mb-3">
                                            <i class="fa fa-chevron-circle-left"></i>
                                        </button>
                                    </a>
                                    <button type="button" class="btn btn-primary mb-3">
                                        ${today}
                                    </button>
                                    <a href="${pageContext.request.contextPath}/ride/list/0/1/${today}">
                                        <button type="button" class="btn btn-primary mb-3">
                                            <i class="fa fa-chevron-circle-right"></i>
                                        </button>
                                    </a>
                                    <button type="button" class="btn btn-success mb-3" data-toggle="modal"
                                            data-target=".modal-add-new"><i class="fa fa-plus"></i> Nowy
                                    </button>
                                </div>

                                <table id="instructorTable" class="table table-striped table-bordered"
                                       style="width:100% !important;">

                                    <thead class="text-uppercase bg-dark" style="width:100% !important;">
                                    <tr class="text-white" style="width:100% !important;">
                                        <th scope="col">Id</th>
                                        <th scope="col">Imię i nazwisko kursanta</th>
                                        <th scope="col">Czas trwania</th>
                                        <th scope="col">Instruktor</th>
                                        <th scope="col">Pojazd</th>
                                        <th scope="col">Typ</th>
                                        <th scope="col">Akcje</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${rides}" var="ride">
                                        <c:if test="${ride.date == today}">
                                            <tr>
                                                <td>${ride.id} (${ride.date})</td>
                                                <td>${ride.client.fullName}</td>
                                                <td>${ride.duration} h</td>
                                                <td>${ride.instructor.fullName}</td>
                                                <td>${ride.car.plate}</td>
                                                <td>${ride.rideType}</td>
                                                <td>
                                                    <div class="float-right">
                                                        <a href="/ride/delete/${ride.id}/${today}">
                                                            <button class="btn btn-danger"><i class="fa fa-trash"></i>
                                                            </button>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </c:if>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="modal fade modal-add-new">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="header-title">Dodaj jazdy</h4>
                </div>
                <form action="${pageContext.request.contextPath}/ride/add/${today}" class="needs-validation" novalidate=""
                      method="post">
                    <div class="modal-body">
                        <%--1 row--%>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="instructorName">Instruktor</label>
                                <select class="selectpicker input-rounded"
                                        name="instructorName"
                                        id="instructorName"
                                        data-live-search="true"
                                        <c:if test="${instr == true}">
                                            disabled
                                        </c:if>
                                        data-max-options="5"
                                        data-width="100%">
                                    <c:forEach items="${instructors}" var="intructor">
                                        <option value="${intructor.id}">
                                                ${intructor.fullName}
                                        </option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label>Kursant</label><br/>
                                <select class="selectpicker input-rounded"
                                        name="client"
                                        id="client"
                                        data-live-search="true"
                                        data-max-options="5"
                                        data-width="100%">
                                    <c:forEach items="${clients}" var="client">
                                        <option value="${client.id}">
                                                ${client.fullName}
                                        </option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label>Pojazd</label><br/>
                                <select class="selectpicker input-rounded"
                                        name="car"
                                        id="car"
                                        data-live-search="true"
                                        data-max-options="5"
                                        data-width="100%">
                                    <c:forEach items="${cars}" var="car">
                                        <option value="${car.plate}">
                                                ${car.plate}
                                        </option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label>Kurs</label><br/>
                                <select class="selectpicker input-rounded"
                                        name="course"
                                        id="course"
                                        data-live-search="true"
                                        data-max-options="5"
                                        data-width="100%">
                                    <c:forEach items="${courses}" var="course">
                                        <option value="${course.id}">
                                                ${course.courseNumber}
                                        </option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="duration">Czas trwania</label>
                                <input type="text" id="duration" name="duration" class="form-control input-rounded"
                                       required>
                                <div class="valid-feedback">
                                    Ok!
                                </div>
                            </div>
                            <div class="col-md-2 mb-3">
                                <label>Typ jazd</label></div>
                            <div class="col-md-2 mb-3">
                                <div class="custom-control custom-radio">
                                    <input type="radio" checked id="customRadi01" name="type" value="0"
                                           class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio1">Normalne</label>
                                </div>
                            </div>
                            <div class="col-md-2 mb-3">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio1" name="type" value="1"
                                           class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio1">Dodatkowe</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>
                        <button class="btn btn-primary" type="submit">Dodaj</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- main content area end -->
    <!-- footer area start-->
    <footer>
        <div class="footer-area">
            <p>© Copyright 2018. All right reserved. Template by <a href="https://colorlib.com/wp/">Colorlib</a>.</p>
        </div>
    </footer>
    <!-- footer area end-->
</div>
<!-- page container area end -->
<%@include file="static/footer.jsp" %>
<script type="application/javascript">

    $(document).ready(function () {
        $('#client').selectpicker();
        $('#car').selectpicker();
        $('#course').selectpicker();
        $('#instructorName').selectpicker();
        $('#instructorTable').DataTable({
            "language": {
                "decimal": "",
                "emptyTable": "Brak danych w tabeli",
                "info": "Rekordy _START_ do _END_ z _TOTAL_",
                "infoEmpty": "Rekordy 0 do 0 z 0 ",
                "infoFiltered": "(filtered from _MAX_ total entries)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Pokaż _MENU_ rekordów",
                "loadingRecords": "Ładowanie...",
                "processing": "Przetwarzanie...",
                "search": "Wyszukaj:",
                "zeroRecords": "Nie znaleziono rekordów",
                "paginate": {
                    "first": "Pierwszy",
                    "last": "Ostatni",
                    "next": "Następny",
                    "previous": "Poprzedni"
                },
                "aria": {
                    "sortAscending": ": sortuj rosnąco",
                    "sortDescending": ": sortuj malejąco"
                }
            }
        });
    });

</script>
</body>

</html>