<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Pojazdy - OSKManager</title>
    <%@include file="static/heads.jsp" %>
</head>

<body>
<!-- preloader area start -->
<div id="preloader">
    <div class="loader"></div>
</div>
<!-- preloader area end -->
<!-- page container area start -->
<div class="page-container">
    <%@include file="static/menu.jsp" %>
    <!-- main content area start -->
    <div class="main-content">
        <!-- header area start -->
        <div class="header-area">
            <div class="row align-items-center">
                <!-- nav and search button -->
                <div class="col-md-6 col-sm-8 clearfix">
                    <div class="nav-btn pull-left">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <!-- profile info & task notification -->
            </div>
        </div>
        <!-- header area end -->
        <!-- page title area start -->
        <div class="page-title-area">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="breadcrumbs-area clearfix">
                        <h4 class="page-title pull-left">Pojazdy</h4>
                        <ul class="breadcrumbs pull-left">
                            <li><a href="${pageContext.request.contextPath}/car/list">Pojazdy</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 clearfix">
                    <div class="user-profile pull-right">
                        <%@ page import="pl.kmo2008.oskmanager.entity.enums.Sex" %>
                        <c:choose>
                            <c:when test="${user.sex == Sex.MAN}">
                                <img class="avatar user-thumb"
                                     src="${pageContext.request.contextPath}/static/images/author/avatar_man.png"
                                     alt="avatar">
                            </c:when>
                            <c:otherwise>
                                <img class="avatar user-thumb"
                                     src="${pageContext.request.contextPath}/static/images/author/avatar_woman.png"
                                     alt="avatar">
                            </c:otherwise>
                        </c:choose>
                        <h4 class="user-name">${user.fullName}</h4>
                    </div>
                </div>
            </div>
        </div>
        <!-- page title area end -->
        <div class="main-content-inner">
            <section>
                <div class="col-lg-12 mt-5">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <div class="float-right mb-5">
                                    <button type="button" class="btn btn-success btn-lg" data-toggle="modal"
                                            data-target=".modal-add-new"><i class="fa fa-plus"></i> Nowy
                                    </button>
                                </div>

                                <table id="instructorTable" class="table table-striped table-bordered"
                                       style="width:100% !important;">

                                    <thead class="text-uppercase bg-dark" style="width:100% !important;">
                                    <tr class="text-white" style="width:100% !important;">
                                        <th style="width: 10%;">Numer rejestracyjny</th>
                                        <th style="width: 10%;">Marka</th>
                                        <th style="width: 10%;">Model</th>
                                        <th style="width: 10%;">Rok produkcji</th>
                                        <th style="width: 10%;">Ubezpieczenie</th>
                                        <th style="width: 10%;">Badanie techniczne</th>
                                        <th style="width: 25%;">Kategorie</th>
                                        <th class="text-center" style="width: 15%;">
                                            <div>
                                                <em class="fa fa-cog"></em>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="car" items="${cars}">
                                        <tr>
                                            <td>${car.plate}</td>
                                            <td>${car.mark}</td>
                                            <td>${car.model}</td>
                                            <td>${car.productionYear}</td>
                                            <td>${car.endOfInsurance.dayOfMonth}.${car.endOfInsurance.monthValue}.${car.endOfInsurance.year}</td>
                                            <td>${car.endOfReview.dayOfMonth}.${car.endOfReview.monthValue}.${car.endOfReview.year}</td>
                                            <td>
                                                <c:forEach items="${car.categories}" var="category">
                                                    ${category.name},
                                                </c:forEach>
                                            </td>
                                            <td>
                                                <div class="float-right">
                                                    <button class='btn btn-sm btn-warning' data-toggle="modal"
                                                            data-target=".modal-edit-${car.id}">
                                                        <span class="fa fa-edit"></span> Edytuj
                                                    </button>
                                                        <%--Edit modal--%>
                                                    <div class="modal fade modal-edit-${car.id}">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <form action="${pageContext.request.contextPath}/car/edit/${car.id}"
                                                                      method="post">
                                                                    <div class="modal-header">
                                                                        <h4 class="header-title">
                                                                            Edycja ${car.plate}</h4>
                                                                    </div>
                                                                    <input type="hidden" name="${_csrf.parameterName}"
                                                                           value="${_csrf.token}"/>
                                                                    <div class="modal-body">
                                                                        <div class="form-row">
                                                                            <div class="col-md-12 mb-3">

                                                                                <label for="plateEdit">Numer
                                                                                    rejestracyjny:</label>
                                                                                <input class="form-control input-rounded"
                                                                                       type="text"
                                                                                       id="plateEdit"
                                                                                       value="${car.plate}"
                                                                                       name="plateEdit">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-row">
                                                                            <div class="col-md-6 mb-3">

                                                                                <label for="reviewEdit">Badanie
                                                                                    techniczne</label>
                                                                                <input type="date" id="reviewEdit"
                                                                                       name="reviewEdit"
                                                                                       class="form-control input-rounded"
                                                                                       value="${car.endOfReview}"
                                                                                       required>
                                                                                <small>(Jeżeli bezterminowo, proszę
                                                                                    wybrać odległy termin)
                                                                                </small>
                                                                                <div class="valid-feedback">
                                                                                    Ok!
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 mb-3">
                                                                                <div class="form-row">
                                                                                    <label for="insuranceEdit">Ubezpieczenie</label>
                                                                                    <input type="date"
                                                                                           id="insuranceEdit"
                                                                                           name="insuranceEdit"
                                                                                           value="${car.endOfInsurance}"
                                                                                           class="form-control input-rounded"
                                                                                           required>
                                                                                    <div class="valid-feedback">
                                                                                        Ok!
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary"
                                                                                data-dismiss="modal">
                                                                            Zamknij
                                                                        </button>
                                                                        <button type="submit" class="btn btn-primary">
                                                                            Zapisz
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                        <%--End of Edit modal--%>
                                                    <button type="button" class="btn btn-danger btn-sm"
                                                            data-toggle="modal"
                                                            data-target=".modal-delete-name"><span
                                                            class="fa fa-trash"></span> Usuń
                                                    </button>
                                                    <div class="modal fade modal-delete-name">
                                                        <div class="modal-dialog modal-sm">
                                                            <div class="modal-content">
                                                                <div class="modal-body">
                                                                    Czy napewno chcesz usunąć <b>${car.plate}</b>?
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary"
                                                                            data-dismiss="modal">
                                                                        Zamknij
                                                                    </button>
                                                                    <a href="${pageContext.request.contextPath}/car/delete/${car.id}"
                                                                       class="btn btn-danger"><span
                                                                            class="fa fa-trash"></span> Usuń</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="modal fade modal-add-new">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="header-title">Dodaj pojazd</h4>
                </div>
                <form action="${pageContext.request.contextPath}/car/add" class="needs-validation" novalidate=""
                      method="post">
                    <div class="modal-body">
                        <%--1 row--%>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="plate">Numer rejestracyjny</label>
                                <input type="text" class="form-control input-rounded" id="plate"
                                       name="plate"
                                       required>
                                <div class="valid-feedback">
                                    Ok.
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="mark">Marka</label>
                                <input type="text" class="form-control input-rounded" id="mark" name="mark" required>
                                <div class="valid-feedback">
                                    Ok!
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="model">Model</label>
                                <input type="text" class="form-control input-rounded" id="model" name="model" required>
                                <div class="valid-feedback">
                                    Ok!
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="productionYear">Rok produkcji</label>
                                <input type="number" class="form-control input-rounded" id="productionYear"
                                       name="productionYear"
                                       required>
                                <div class="valid-feedback">
                                    Ok!
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">

                                <label for="review">Badanie techniczne</label>
                                <input type="date" id="review" name="review" class="form-control input-rounded"
                                       required>
                                <small>(Jeżeli bezterminowo, proszę wybrać odległy termin)</small>
                                <div class="valid-feedback">
                                    Ok!
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="form-row">
                                    <label for="insurance">Ubezpieczenie</label>
                                    <input type="date" id="insurance" name="insurance"
                                           class="form-control input-rounded"
                                           required>
                                    <div class="valid-feedback">
                                        Ok!
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <b class="text-muted mb-3 mt-4 d-block">Kategorie:</b>
                                <c:forEach items="${categories}" var="category">
                                    <div class="custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" id="${category.name}" name="${category.name}"
                                               class="custom-control-input">
                                        <label class="custom-control-label"
                                               for="${category.name}">${category.name}</label>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>
                        <button class="btn btn-primary" type="submit">Dodaj</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- main content area end -->
    <!-- footer area start-->
    <footer>
        <div class="footer-area">
            <p>© Copyright 2018. All right reserved. Template by <a href="https://colorlib.com/wp/">Colorlib</a>.</p>
        </div>
    </footer>
    <!-- footer area end-->
</div>
<!-- page container area end -->
<%@include file="static/footer.jsp" %>
</body>

</html>