<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Transkacje - OSKManager</title>
    <%@include file="static/heads.jsp" %>
</head>

<body>
<!-- preloader area start -->
<div id="preloader">
    <div class="loader"></div>
</div>
<!-- preloader area end -->
<!-- page container area start -->
<div class="page-container">
    <%@include file="static/menu.jsp" %>
    <!-- main content area start -->
    <div class="main-content">
        <!-- header area start -->
        <div class="header-area">
            <div class="row align-items-center">
                <!-- nav and search button -->
                <div class="col-md-6 col-sm-8 clearfix">
                    <div class="nav-btn pull-left">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <!-- profile info & task notification -->
            </div>
        </div>
        <!-- header area end -->
        <!-- page title area start -->
        <div class="page-title-area">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="breadcrumbs-area clearfix">
                        <h4 class="page-title pull-left">Strona Główna</h4>
                        <ul class="breadcrumbs pull-left">
                            <li><a href="${pageContext.request.contextPath}/dashboard">Ewidencja</a></li>
                            <li>Transkacje</li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 clearfix">
                    <div class="user-profile pull-right">
                        <%@ page import="pl.kmo2008.oskmanager.entity.enums.Sex" %>
                        <c:choose>
                            <c:when test="${user.sex == Sex.MAN}">
                                <img class="avatar user-thumb"
                                     src="${pageContext.request.contextPath}/static/images/author/avatar_man.png"
                                     alt="avatar">
                            </c:when>
                            <c:otherwise>
                                <img class="avatar user-thumb"
                                     src="${pageContext.request.contextPath}/static/images/author/avatar_woman.png"
                                     alt="avatar">
                            </c:otherwise>
                        </c:choose>
                        <h4 class="user-name">${user.fullName}</h4>
                    </div>
                </div>
            </div>
        </div>
        <!-- page title area end -->
        <div class="main-content-inner">
            <section>
                <div class="col-lg-12 mt-5">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <div class="float-right mb-5">
                                    <c:set var="sum" value="0"/>
                                    <c:forEach var="transaction" items="${transactions}">
                                        <c:if test="${transaction.date == today}">
                                            <c:set var="sum" value="${sum + transaction.value}"/>
                                        </c:if>
                                    </c:forEach>

                                    <button type="button" class="btn btn-success mb-3">
                                        ${sum} zł
                                    </button> &nbsp;
                                    <a href="${pageContext.request.contextPath}/transaction/list/1/0/${today}">
                                        <button type="button" class="btn btn-primary mb-3">
                                            <i class="fa fa-chevron-circle-left"></i>
                                        </button>
                                    </a>
                                    <button type="button" class="btn btn-primary mb-3">
                                        ${today}
                                    </button>
                                    <a href="${pageContext.request.contextPath}/transaction/list/0/1/${today}">
                                        <button type="button" class="btn btn-primary mb-3">
                                            <i class="fa fa-chevron-circle-right"></i>
                                        </button>
                                    </a>
                                </div>

                                <table id="instructorTable" class="table table-striped table-bordered"
                                       style="width:100% !important;">

                                    <thead class="text-uppercase bg-dark" style="width:100% !important;">
                                    <tr class="text-white" style="width:100% !important;">
                                        <th style="width: 3%;">Id</th>
                                        <th style="width: 10%;">Imię i nazwisko klienta</th>
                                        <th style="width: 10%;">Wartość</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="transaction" items="${transactions}">
                                        <c:if test="${transaction.date == today}">
                                            <tr>
                                                <td>${transaction.id}</td>
                                                <td>${transaction.client.fullName}</td>
                                                <td>${transaction.value}</td>
                                            </tr>
                                        </c:if>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- main content area end -->
    <!-- footer area start-->
    <footer>
        <div class="footer-area">
            <p>© Copyright 2018. All right reserved. Template by <a href="https://colorlib.com/wp/">Colorlib</a>.</p>
        </div>
    </footer>
    <!-- footer area end-->
</div>
<!-- page container area end -->
<%@include file="static/footer.jsp" %>
<script type="application/javascript">
    $(document).ready(function () {
        $('#instructorTable').DataTable({
            "language": {
                "decimal": "",
                "emptyTable": "Brak danych w tabeli",
                "info": "Rekordy _START_ do _END_ z _TOTAL_",
                "infoEmpty": "Rekordy 0 do 0 z 0 ",
                "infoFiltered": "(filtered from _MAX_ total entries)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Pokaż _MENU_ rekordów",
                "loadingRecords": "Ładowanie...",
                "processing": "Przetwarzanie...",
                "search": "Wyszukaj:",
                "zeroRecords": "Nie znaleziono rekordów",
                "paginate": {
                    "first": "Pierwszy",
                    "last": "Ostatni",
                    "next": "Następny",
                    "previous": "Poprzedni"
                },
                "aria": {
                    "sortAscending": ": sortuj rosnąco",
                    "sortDescending": ": sortuj malejąco"
                }
            }
        });
    });
</script>
</body>

</html>