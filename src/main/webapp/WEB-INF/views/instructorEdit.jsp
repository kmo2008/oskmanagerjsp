<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>${instructor.fullName} - OSKManager</title>
    <%@include file="static/heads.jsp" %>
</head>

<body>
<!-- preloader area start -->
<div id="preloader">
    <div class="loader"></div>
</div>
<!-- preloader area end -->
<!-- page container area start -->
<div class="page-container">
    <%@include file="static/menu.jsp" %>
    <!-- main content area start -->
    <div class="main-content">
        <!-- header area start -->
        <div class="header-area">
            <div class="row align-items-center">
                <!-- nav and search button -->
                <div class="col-md-6 col-sm-8 clearfix">
                    <div class="nav-btn pull-left">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <!-- profile info & task notification -->
            </div>
        </div>
        <!-- header area end -->
        <!-- page title area start -->
        <div class="page-title-area">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="breadcrumbs-area clearfix">
                        <h4 class="page-title pull-left">${instructor.fullName} ( ${instructor.instructorNumber})
                            <button type="button" class="btn btn-warning btn-xs mb-3" data-toggle="modal"
                                    data-target=".modal-edit-name">Zmień
                            </button>
                            <div class="modal fade modal-edit-name">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <form action="${pageContext.request.contextPath}/instructor/edit/${instructor.id}/name"
                                              method="post">
                                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                            <div class="modal-body">
                                                <div class="col-md-12 mb-3">
                                                    <div class="form-row">
                                                        <label for="firstName">Imię:</label>
                                                        <input class="form-control input-rounded" type="text"
                                                               id="firstName"
                                                               value="${instructor.name}"
                                                               name="firstName">
                                                    </div>
                                                    <div class="form-row">
                                                        <label for="secondName">Drugie imię:</label>
                                                        <input class="form-control input-rounded" type="text"
                                                               id="secondName"
                                                               value="${instructor.secondName}"
                                                               name="secondName">
                                                    </div>
                                                    <div class="form-row">
                                                        <label for="lastName">Nazwisko:</label>
                                                        <input class="form-control input-rounded" type="text"
                                                               id="lastName"
                                                               value="${instructor.lastName}"
                                                               name="lastName">
                                                    </div>
                                                    <div class="form-row">
                                                        <label for="instructorNumber">Numer instruktora:</label>
                                                        <input class="form-control input-rounded" type="text"
                                                               id="instructorNumber"
                                                               value="${instructor.instructorNumber}"
                                                               name="instructorNumber">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                    Zamknij
                                                </button>
                                                <button type="submit" class="btn btn-primary">Zapisz</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </h4>
                        <ul class="breadcrumbs pull-left">
                            <li><a href="${pageContext.request.contextPath}/dashboard">Ewidencja</a></li>
                            <li><a href="${pageContext.request.contextPath}/instructor/list">Instruktorzy</a></li>
                            <li>${instructor.fullName}</li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 clearfix">
                    <div class="user-profile pull-right">
                        <%@ page import="pl.kmo2008.oskmanager.entity.enums.Sex" %>
                        <c:choose>
                            <c:when test="${user.sex == Sex.MAN}">
                                <img class="avatar user-thumb"
                                     src="${pageContext.request.contextPath}/static/images/author/avatar_man.png"
                                     alt="avatar">
                            </c:when>
                            <c:otherwise>
                                <img class="avatar user-thumb"
                                     src="${pageContext.request.contextPath}/static/images/author/avatar_woman.png"
                                     alt="avatar">
                            </c:otherwise>
                        </c:choose>
                        <h4 class="user-name">${user.fullName}</h4>
                    </div>
                </div>
            </div>
        </div>
        <!-- page title area end -->
        <div class="main-content-inner">
            <section>
                <div class="mt-5">
                    <div class="card">
                        <div class="card-body">
                            <div class="container-fluid">
                                <form action="${pageContext.request.contextPath}/instructor/edit/${instructor.id}/all"
                                      method="post">
                                    <input type="hidden" name="name" value="${instructor.name}">
                                    <input type="hidden" name="secondName" value="${instructor.secondName}">
                                    <input type="hidden" name="lastName" value="${instructor.lastName}">
                                    <input type="hidden" name="instructorNumber" value="${instructor.instructorNumber}">
                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                    <div class="row">
                                        <div class="col-auto ml-auto">
                                            <button type="submit" class="btn btn-success">Zapisz</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <%--3 row--%>
                                        <div class="col-4 mb-3">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control input-rounded" id="email"
                                                   value="${instructor.email}"
                                                   name="email"
                                                   required="">
                                            <div class="invalid-feedback">
                                                Niepoprawny adres email.
                                            </div>
                                        </div>
                                        <div class="col-4 mb-3">
                                            <label for="pesel">Pesel</label>
                                            <input type="text" class="form-control input-rounded" id="pesel"
                                                   name="pesel"
                                                   value="${instructor.pesel}"
                                                   pattern="[0-9]{11}" required>
                                            <div class="invalid-feedback">
                                                Niepoprawny PESEL.
                                            </div>
                                        </div>
                                        <div class="col-4 mb-3">
                                            <label>Płeć</label>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" checked id="sexM" name="sex"
                                                       class="custom-control-input">
                                                <label class="custom-control-label" for="sexM">Mężczyzna</label>
                                            </div>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="sexW" name="sex" class="custom-control-input">
                                                <label class="custom-control-label" for="sexW">Kobieta</label>
                                            </div>
                                        </div>
                                    </div>
                                    <%--4 row--%>
                                    <div class="row">
                                        <div class="col-4 mb-3">
                                            <label for="street">Ulica</label>
                                            <input type="text" class="form-control input-rounded" id="street"
                                                   name="street"
                                                   value="${instructor.street}"
                                                   required>
                                            <div class="valid-feedback">
                                                Ok!
                                            </div>
                                        </div>
                                        <div class="col-4 mb-3">
                                            <label for="houseNumber">Numer domu</label>
                                            <input type="text" class="form-control input-rounded" id="houseNumber"
                                                   value="${instructor.houseNumber}"
                                                   name="houseNumber" required>
                                            <div class="valid-feedback">
                                                Ok!
                                            </div>
                                        </div>
                                        <div class="col-4 mb-3">
                                            <label for="apartmentNumber">Numer mieszkania</label>
                                            <input type="text" class="form-control input-rounded" id="apartmentNumber"
                                                   value="${instructor.apartmentNumber}"
                                                   name="apartmentNumber">

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4 mb-2">
                                            <label for="document" class="col-form-label">Dokument</label>
                                            <select class="form-control input-rounded" style="height: 50px !important;"
                                                    id="document" name="document">
                                                <option value="ID">Dowód osobisty</option>
                                                <option value="PASSPORT">Paszport</option>
                                                <option value="OTHER">Inny</option>
                                            </select>
                                        </div>
                                        <div class="col-4 mb-3">
                                            <label for="documentNumber">Numer dokumentu</label>
                                            <input type="text" class="form-control input-rounded" id="documentNumber"
                                                   value="${instructor.documentNumber}"
                                                   name="documentNumber" required>
                                            <div class="valid-feedback">
                                                Ok!
                                            </div>
                                        </div>
                                        <div class="col-4 mb-3">
                                            <label for="phone">Telefon</label>
                                            <input type="text" class="form-control input-rounded" id="phone"
                                                   value="${instructor.phone}"
                                                   name="phone" required>
                                            <div class="valid-feedback">
                                                Ok!
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4 mb-3">
                                            <label class="col-form-label" for="normalPayment">Stawka za godzinę
                                                zwykłą.</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control input-rounded" id="normalPayment"
                                                       value="${instructor.normalPayment}"
                                                       name="normalPayment" required>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">zł</span>
                                                </div>
                                                <div class="valid-feedback">
                                                    Ok!
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4 mb-3">
                                            <label class="col-form-label" for="additionalPayment">Stawka za godzinę
                                                dodatkową</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control input-rounded"
                                                       id="additionalPayment"
                                                       value="${instructor.additionalPayment}"
                                                       name="additionalPayment" required>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">zł</span>
                                                </div>
                                                <div class="valid-feedback">
                                                    Ok!
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 mb-3">
                                            <b class="text-muted mb-3 mt-4 d-block">Kategorie:</b>
                                            <c:forEach items="${categories}" var="category">
                                                <div class="custom-control custom-checkbox custom-control-inline">
                                                    <input type="checkbox" id="${category.name}" name="${category.name}"
                                                           class="custom-control-input">
                                                    <label class="custom-control-label"
                                                           for="${category.name}">${category.name}</label>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-auto ml-auto  mt-5">
                                            <button type="button" class="btn btn-danger mb-3" data-toggle="modal"
                                                    data-target=".modal-delete-name"><span
                                                    class="fa fa-trash"></span> Usuń
                                            </button>
                                            <div class="modal fade modal-delete-name">
                                                <div class="modal-dialog modal-sm">
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            Czy napewno chcesz usunąć <b>${instructor.fullName}</b>?
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">
                                                                Zamknij
                                                            </button>
                                                            <a href="${pageContext.request.contextPath}/instructor/delete/${instructor.id}"
                                                               class="btn btn-danger"><span
                                                                    class="fa fa-trash"></span> Usuń</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section>
                <div class="col-lg-12 mt-5">
                    <div class="card pl-3 pt-3">
                        <div class="card-title">
                            <h4>Wypłata</h4>
                        </div>
                        <div class="card-body">
                            <div class="single-table">
                                <div class="table-responsive">
                                    <table class="table text-center">
                                        <thead class="text-uppercase bg-secondary">
                                        <tr class="text-white">
                                            <th scope="col">Miesiąc</th>
                                            <th scope="col">Wypłata</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:forEach items="${rides}" var="month">
                                            <tr>
                                                <td>${month.key}</td>
                                                <td>${month.value} zł</td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>


    <!-- main content area end -->
    <!-- footer area start-->
    <footer>
        <div class="footer-area">
            <p>© Copyright 2018. All right reserved. Template by <a href="https://colorlib.com/wp/">Colorlib</a>.</p>
        </div>
    </footer>
    <!-- footer area end-->
</div>
<!-- page container area end -->
<%@include file="static/footer.jsp" %>

<div id="jsVariables"
     data-sex="${instructor.sex}"
     data-document="${instructor.document}"
     data-cats='${instructor.categories}'
>
</div>
</body>


<script type="application/javascript">
</script>

</html>