<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!doctype html>
<html class="no-js" lang="pl">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Instruktorzy - OSKManager</title>
    <%@include file="static/heads.jsp" %>
</head>

<body>
<!-- preloader area start -->
<div id="preloader">
    <div class="loader"></div>
</div>
<!-- preloader area end -->
<!-- page container area start -->
<div class="page-container">
    <%@include file="static/menu.jsp" %>
    <!-- main content area start -->
    <div class="main-content">
        <!-- header area start -->
        <div class="header-area">
            <div class="row align-items-center">
                <!-- nav and search button -->
                <div class="col-md-6 col-sm-8 clearfix">
                    <div class="nav-btn pull-left">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
        <!-- header area end -->
        <!-- page title area start -->
        <div class="page-title-area">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="breadcrumbs-area clearfix">
                        <h4 class="page-title pull-left">Instruktorzy</h4>
                        <ul class="breadcrumbs pull-left">
                            <li><a href="javascript:void(0)">Ewidencja</a></li>
                            <li><span>Instruktorzy</span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 clearfix">
                    <div class="user-profile pull-right">
                        <%@ page import="pl.kmo2008.oskmanager.entity.enums.Sex" %>
                        <c:choose>
                            <c:when test="${user.sex == Sex.MAN}">
                                <img class="avatar user-thumb"
                                     src="${pageContext.request.contextPath}/static/images/author/avatar_man.png"
                                     alt="avatar">
                            </c:when>
                            <c:otherwise>
                                <img class="avatar user-thumb"
                                     src="${pageContext.request.contextPath}/static/images/author/avatar_woman.png"
                                     alt="avatar">
                            </c:otherwise>
                        </c:choose>
                        <h4 class="user-name">${user.fullName}</h4>
                    </div>
                </div>
            </div>
        </div>
        <!-- page title area end -->
        <div class="main-content-inner">
            <section>
                <div class="col-lg-12 mt-5">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <div class="float-right mb-5">
                                    <button type="button" class="btn btn-success btn-lg" data-toggle="modal"
                                            data-target=".modal-add-new"><i class="fa fa-plus"></i> Nowy
                                    </button>
                                </div>

                                <table id="instructorTable" class="table table-striped table-bordered"
                                       style="width:100% !important;">

                                    <thead class="text-uppercase bg-dark" style="width:100% !important;">
                                    <tr class="text-white" style="width:100% !important;">
                                        <th style="width: 10%;">Numer instruktora</th>
                                        <th style="width: 10%;">Imię i nazwisko</th>
                                        <th style="width: 10%;">Email</th>
                                        <th style="width: 10%;">Domyślne hasło</th>
                                        <th style="width: 20%;">Uprawnienia</th>
                                        <th style="width: 10%;">Telefon</th>
                                        <th style="width: 5%;">Konto aktywne</th>
                                        <th class="text-center" style="width: 25%;">
                                            <div>
                                                <em class="fa fa-cog"></em>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="instructor" items="${instructors}">
                                        <tr>
                                            <td>${instructor.instructorNumber}</td>
                                            <td scope="col">${instructor.fullName}</td>
                                            <td scope="col">${instructor.email}</td>
                                            <td scope="col">${instructor.defaultPassword}</td>
                                            <td>
                                                <c:forEach items="${instructor.categories}" var="category">
                                                    ${category.name},
                                                </c:forEach>
                                            </td>
                                            <td><c:set var="phone" value="${instructor.phone}"/>
                                                <fmt:formatNumber value="${phone}" pattern="000,000,000" var="result"/>
                                                    ${fn:replace(result, ",", "-")}</td>
                                            <td><c:if test="${instructor.active == 1}">Tak</c:if><c:if
                                                    test="${instructor.active == 0}">Nie</c:if></td>
                                            <td>
                                                <div class="float-right">
                                                    <a class='btn btn-sm btn-warning'
                                                       href="${pageContext.request.contextPath}/instructor/edit/${instructor.id}">
                                                        <span class="fa fa-edit"></span> Edytuj</a>
                                                    <a class='btn btn-sm btn-primary'
                                                       href="${pageContext.request.contextPath}/instructor/active/${instructor.id}">
                                                        <span class="fa fa-edit"></span> <c:if
                                                            test="${instructor.active == 1}">Deaktywuj</c:if><c:if
                                                            test="${instructor.active == 0}">Aktywuj</c:if></a>

                                                    <button type="button" class="btn btn-danger btn-sm"
                                                            data-toggle="modal"
                                                            data-target=".modal-delete-${instructor.id}"><span
                                                            class="fa fa-trash"></span> Usuń
                                                    </button>
                                                    <div class="modal fade modal-delete-${instructor.id}">
                                                        <div class="modal-dialog modal-sm">
                                                            <div class="modal-content">
                                                                <div class="modal-body">
                                                                    Czy napewno chcesz usunąć
                                                                    <b>${instructor.fullName}</b>?
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary"
                                                                            data-dismiss="modal">
                                                                        Zamknij
                                                                    </button>
                                                                    <a href="${pageContext.request.contextPath}/instructor/delete/${instructor.id}"
                                                                       class="btn btn-danger"><span
                                                                            class="fa fa-trash"></span> Usuń</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <%--Add modal start--%>
    <div class="modal fade modal-add-new">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="header-title">Dodaj instruktora</h4>
                </div>
                <form action="${pageContext.request.contextPath}/instructor/add" class="needs-validation" novalidate=""
                      method="post">
                    <div class="modal-body">
                        <%--1 row--%>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="instructorNumber">Numer instruktora</label>
                                <input type="text" class="form-control input-rounded" id="instructorNumber"
                                       name="instructorNumber"
                                       required>
                                <div class="invalid-feedback">
                                    Niepoprawny numer instruktora.
                                </div>
                            </div>
                        </div>
                        <%--2 row--%>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="name">Imię</label>
                                <input type="text" class="form-control input-rounded" id="name" name="name" required>
                                <div class="valid-feedback">
                                    Ok!
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="secondName">Drugie imię</label>
                                <input type="text" class="form-control input-rounded" id="secondName" name="secondName">
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="lastName">Nazwisko</label>
                                <input type="text" class="form-control input-rounded" id="lastName" name="lastName"
                                       required>
                                <div class="valid-feedback">
                                    Ok!
                                </div>
                            </div>
                        </div>
                        <%--3 row--%>
                        <div class="form-row">
                            <div class="col-md-5 mb-3">
                                <label for="email">Email</label>
                                <input type="email" class="form-control input-rounded" id="email" name="email"
                                       required="">
                                <div class="invalid-feedback">
                                    Niepoprawny adres email.
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="pesel">Pesel</label>
                                <input type="text" class="form-control input-rounded" id="pesel" name="pesel"
                                       pattern="[0-9]{11}" required>
                                <div class="invalid-feedback">
                                    Niepoprawny PESEL.
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label>Płeć</label>
                                <div class="custom-control custom-radio">
                                    <input type="radio" checked id="sexM" name="sex" value="MAN"
                                           class="custom-control-input">
                                    <label class="custom-control-label" for="sexM">Mężczyzna</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="sexW" name="sex" value="WOMAN" class="custom-control-input">
                                    <label class="custom-control-label" for="sexW">Kobieta</label>
                                </div>
                            </div>
                        </div>
                        <%--4 row--%>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="street">Ulica</label>
                                <input type="text" class="form-control input-rounded" id="street" name="street"
                                       required>
                                <div class="valid-feedback">
                                    Ok!
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="houseNumber">Numer domu</label>
                                <input type="text" class="form-control input-rounded" id="houseNumber"
                                       name="houseNumber" required>
                                <div class="valid-feedback">
                                    Ok!
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="apartmentNumber">Numer mieszkania</label>
                                <input type="text" class="form-control input-rounded" id="apartmentNumber"
                                       name="apartmentNumber">

                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label class="col-form-label">Dokument</label>
                                <select class="form-control input-rounded" name="document">
                                    <option>Dowód osobisty</option>
                                    <option>Paszport</option>
                                    <option>Inny</option>
                                </select>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="documentNumber">Numer dokumentu</label>
                                <input type="text" class="form-control input-rounded" id="documentNumber"
                                       name="documentNumber" required>
                                <div class="valid-feedback">
                                    Ok!
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="phone">Telefon</label>
                                <input type="text" class="form-control input-rounded" id="phone"
                                       name="phone" required>
                                <div class="valid-feedback">
                                    Ok!
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label class="col-form-label" for="normalPayment">Stawka za godzinę zwykłą.</label>
                                <div class="input-group">
                                    <input type="text" class="form-control input-rounded" id="normalPayment"
                                           name="normalPayment" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text">zł</span>
                                    </div>
                                    <div class="valid-feedback">
                                        Ok!
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label class="col-form-label" for="additionalPayment">Stawka za godzinę
                                    dodatkową</label>
                                <div class="input-group">
                                    <input type="text" class="form-control input-rounded" id="additionalPayment"
                                           name="additionalPayment" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text">zł</span>
                                    </div>
                                    <div class="valid-feedback">
                                        Ok!
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <b class="text-muted mb-3 mt-4 d-block">Kategorie:</b>
                                <c:forEach items="${categories}" var="category">
                                    <div class="custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" id="${category.name}" name="${category.name}"
                                               class="custom-control-input">
                                        <label class="custom-control-label"
                                               for="${category.name}">${category.name}</label>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>
                        <button class="btn btn-primary" type="submit">Dodaj</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%--Add modal end--%>
    <!-- main content area end -->
    <!-- footer area start-->
    <footer>
        <div class="footer-area">
            <p>© Copyright 2018. All right reserved. Template by <a href="https://colorlib.com/wp/">Colorlib</a>.</p>
        </div>
    </footer>
    <!-- footer area end-->
</div>
<%--FAB--%>

<%--FAB end--%>
<!-- page container area end -->
<%@include file="static/footer.jsp" %>
<script type="application/javascript">
    $(document).ready(function () {
        $('#instructorTable').DataTable({
            "language": {
                "decimal": "",
                "emptyTable": "Brak danych w tabeli",
                "info": "Rekordy _START_ do _END_ z _TOTAL_",
                "infoEmpty": "Rekordy 0 do 0 z 0 ",
                "infoFiltered": "(filtered from _MAX_ total entries)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Pokaż _MENU_ rekordów",
                "loadingRecords": "Ładowanie...",
                "processing": "Przetwarzanie...",
                "search": "Wyszukaj:",
                "zeroRecords": "Nie znaleziono rekordów",
                "paginate": {
                    "first": "Pierwszy",
                    "last": "Ostatni",
                    "next": "Następny",
                    "previous": "Poprzedni"
                },
                "aria": {
                    "sortAscending": ": sortuj rosnąco",
                    "sortDescending": ": sortuj malejąco"
                }
            }
        });
    });
</script>
</body>

</html>