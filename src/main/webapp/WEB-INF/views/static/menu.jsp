<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="sidebar-menu">
    <div class="sidebar-header">
        <div class="logo" style="font-size: 7em;">
            <a href="${pageContext.request.contextPath}/dashboard"><i class="fa fa-automobile fa-10x"
                                                                      style="color: white;"></i></a>
        </div>
    </div>
    <div class="main-menu">
        <div class="menu-inner">
            <nav>
                <ul class="metismenu text-white" id="menu">
                    <li>
                        <a href="${pageContext.request.contextPath}/dashboard"><i class="ti-dashboard"></i><span>Strona Główna</span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" aria-expanded="true"><i
                                class="fa fa-book"></i><span>Ewidencja</span></a>
                        <ul class="collapse">
                            <li><a href="${pageContext.request.contextPath}/client/list"><i
                                    class="fa fa-graduation-cap"></i><span>Kursanci</span></a></li>
                            <li><a href="${pageContext.request.contextPath}/instructor/list"><i
                                    class="fa fa-users"></i><span>Instruktorzy</span></a></li>
                            <li><a href="${pageContext.request.contextPath}/transaction/list"><i class="fa fa-money"></i><span>Transakcje</span></a></li>
                            <li><a href="${pageContext.request.contextPath}/course/list"><i class="fa fa-group"></i><span>Kursy</span></a></li>
                            <li><a href="${pageContext.request.contextPath}/ride/list"><i class="fa fa-road"></i><span>Jazdy</span></a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/car/list"><i
                                class="fa fa-automobile"></i><span>Pojazdy</span></a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/user/changePassword"><i
                                class="fa fa-lock"></i><span>Zmień hasło</span></a>
                    </li>
                    <li><a class="dropdown-item" href="${pageContext.request.contextPath}/logout"><i
                            class="fa fa-sign-out"></i><span>Wyloguj</span></a></li>
                </ul>
            </nav>
        </div>
    </div>
</div>
<!-- sidebar menu area end -->