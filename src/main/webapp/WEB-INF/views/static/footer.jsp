<!-- jquery latest version -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<!-- bootstrap 4 js -->
<script src="${pageContext.request.contextPath}/static/js/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/owl.carousel.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/metisMenu.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/jquery.slimscroll.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/jquery.slicknav.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
<!-- start chart js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
<!-- start highcharts js -->
<script src="https://code.highcharts.com/highcharts.js"></script>

<!-- all line chart activation -->
<script src="${pageContext.request.contextPath}/static/js/line-chart.js"></script>
<!-- all pie chart -->
<script src="${pageContext.request.contextPath}/static/js/pie-chart.js"></script>
<!-- others plugins -->
<script src="${pageContext.request.contextPath}/static/js/plugins.js"></script>
<script src="${pageContext.request.contextPath}/static/js/scripts.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
