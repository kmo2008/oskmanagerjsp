<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png" href="${pageContext.request.contextPath}/static/images/icon/favicon.ico">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/font-awesome.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/themify-icons.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/metisMenu.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/owl.carousel.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/slicknav.min.css">
<!-- amchart css -->
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all"/>
<!-- others css -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/typography.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/default-css.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/styles.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/responsive.css">
<!-- modernizr css -->
<script src="${pageContext.request.contextPath}/static/js/vendor/modernizr-2.8.3.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/include.css">
