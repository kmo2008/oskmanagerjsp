<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Strona Główna - OSKManager</title>
    <%@include file="static/heads.jsp" %>
</head>

<body>
<!-- preloader area start -->
<div id="preloader">
    <div class="loader"></div>
</div>
<!-- preloader area end -->
<!-- page container area start -->
<div class="page-container">
    <%@include file="static/menu.jsp" %>


    <!-- main content area start -->
    <div class="main-content">
        <!-- header area start -->
        <div class="header-area">
            <div class="row align-items-center">
                <!-- nav and search button -->
                <div class="col-md-6 col-sm-8 clearfix">
                    <div class="nav-btn pull-left">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
        <!-- header area end -->
        <!-- page title area start -->
        <div class="page-title-area">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="breadcrumbs-area clearfix">
                        <h4 class="page-title pull-left">Strona Główna</h4>
                        <ul class="breadcrumbs pull-left">
                            <li><a href="${pageContext.request.contextPath}/dashboard">Strona Główna</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 clearfix">
                    <div class="user-profile pull-right">
                        <%@ page import="pl.kmo2008.oskmanager.entity.enums.Sex" %>
                        <c:choose>
                            <c:when test="${user.sex == Sex.MAN}">
                                <img class="avatar user-thumb"
                                     src="${pageContext.request.contextPath}/static/images/author/avatar_man.png"
                                     alt="avatar">
                            </c:when>
                            <c:otherwise>
                                <img class="avatar user-thumb"
                                     src="${pageContext.request.contextPath}/static/images/author/avatar_woman.png"
                                     alt="avatar">
                            </c:otherwise>
                        </c:choose>
                        <h4 class="user-name">${user.fullName}</h4>
                    </div>
                </div>
            </div>
        </div>
        <!-- page title area end -->
        <div class="main-content-inner">
            <%--Tutaj dodać treść--%>
            <c:choose>
                <c:when test="${isClient}">
                    <%--Wygląd jeśli jest klientem--%>
                </c:when>
                <c:otherwise>
                    <section>
                        <div class="alert-area">
                            <div class="row">
                                <div class="col-lg-6 mt-5">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="header-title">Badania techniczne pojazdów</h4>
                                            <div class="additional-content">
                                                <c:forEach items="${ReviewAlert}" var="alert">
                                                    <div class="alert alert-${alert.style}" role="alert">
                                                        <h6>${alert.date.dayOfMonth}.${alert.date.monthValue}.${alert.date.year}<a
                                                                href="/alert/delete/${alert.id}"><span
                                                                class="fa fa-times close"></span></a></h6>
                                                        <h4 class="alert-heading">${alert.title}</h4>
                                                        <p>${alert.description}</p>
                                                    </div>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- additional content area end -->
                                </div>
                                <div class="col-lg-6 mt-5">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="header-title">Ubezpieczenie pojazdów</h4>
                                            <div class="additional-content">
                                                <c:forEach items="${InsuranceAlert}" var="alert">
                                                    <div class="alert alert-${alert.style}" role="alert">
                                                        <h6>${alert.date.dayOfMonth}.${alert.date.monthValue}.${alert.date.year}<a
                                                                href="/alert/delete/${alert.id}"><span
                                                                class="fa fa-times close"></span></a></h6>
                                                        <h4 class="alert-heading">${alert.title}</h4>
                                                        <p>${alert.description}</p>
                                                    </div>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- additional content area end -->
                                </div>

                            </div>
                        </div>
                    </section>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
    <!-- main content area end -->
    <!-- footer area start-->
    <footer>
        <div class="footer-area">
            <p>© Copyright 2018. All right reserved. Template by <a href="https://colorlib.com/wp/">Colorlib</a>.</p>
        </div>
    </footer>
    <!-- footer area end-->
</div>
<!-- page container area end -->
<%@include file="static/footer.jsp" %>
</body>

</html>