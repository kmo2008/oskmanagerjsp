<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html class="no-js" lang="pl">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Login - OSKManager</title>
    <%@include file="static/heads.jsp" %>
</head>


<c:url var="loginUrl" value="/login"/>


<body>
<!-- preloader area start -->
<div id="preloader">
    <div class="loader"></div>
</div>
<!-- preloader area end -->
<!-- login area start -->
<div class="login-area login-s2">
    <div class="container">
        <div class="login-box ptb--100">
            <form action="${loginUrl}" method="post" accept-charset="UTF-8" role="form">
                <div class="login-form-head">
                    <h4>Zaloguj sie</h4>
                    <p>Witaj, zaloguj się aby uzyskać odstęp do systemu.</p>
                </div>
                <div class="login-form-body">
                    <div class="form-gp">
                        <label for="ssoId">Email</label>
                        <input type="email" id="ssoId" name="ssoId">
                        <i class="ti-email"></i>
                    </div>
                    <div class="form-gp">
                        <label for="password">Hasło</label>
                        <input type="password" id="password" name="password">
                        <i class="ti-lock"></i>
                    </div>
                    <div class="submit-btn-area">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <button id="form_submit" type="submit">Zaloguj <i class="ti-arrow-right"></i></button>
                    </div>
                </div>
                <c:if test="${param.error != null}">
                    <div class="alert alert-danger">
                        <p>
                            <b>Błąd logowania</b>
                        </p>
                        <p>Przyczyny:
                        <ul>
                            <li>Podano nieprawidłowe dane</li>
                            <li>Twoje konto nie jest aktywne</li>
                        </ul>
                        </p>
                        <p><small>Aby aktywować konto skontaktuj się z <b>OSK</b>.</small></p>
                    </div>
                </c:if>
                <c:if test="${param.logout != null}">
                    <div class="alert alert-success">
                        <p>Wylogowano pomyślnie.</p>
                    </div>
                </c:if>
            </form>
        </div>
    </div>
</div>
<!-- login area end -->

<%@include file="static/footer.jsp" %>
</body>

</html>