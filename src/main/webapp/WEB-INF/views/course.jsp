<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Kursy - OSKManager</title>
    <%@include file="static/heads.jsp"%>
</head>

<body>
<!-- preloader area start -->
<div id="preloader">
    <div class="loader"></div>
</div>
<!-- preloader area end -->
<!-- page container area start -->
<div class="page-container">
    <%@include file="static/menu.jsp" %>
    <!-- main content area start -->
    <div class="main-content">
        <!-- header area start -->
        <div class="header-area">
            <div class="row align-items-center">
                <!-- nav and search button -->
                <div class="col-md-6 col-sm-8 clearfix">
                    <div class="nav-btn pull-left">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <!-- profile info & task notification -->
            </div>
        </div>
        <!-- header area end -->
        <!-- page title area start -->
        <div class="page-title-area">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="breadcrumbs-area clearfix">
                        <h4 class="page-title pull-left">Kursy</h4>
                        <ul class="breadcrumbs pull-left">
                            <li><a href="javascript:void(0)">Ewidencja</a></li>
                            <li><span>Kursy</span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 clearfix">
                    <div class="user-profile pull-right">
                        <%@ page import="pl.kmo2008.oskmanager.entity.enums.Sex" %>
                        <c:choose>
                            <c:when test="${user.sex == Sex.MAN}">
                                <img class="avatar user-thumb" src="${pageContext.request.contextPath}/static/images/author/avatar_man.png" alt="avatar">
                            </c:when>
                            <c:otherwise>
                                <img class="avatar user-thumb" src="${pageContext.request.contextPath}/static/images/author/avatar_woman.png" alt="avatar">
                            </c:otherwise>
                        </c:choose>
                        <h4 class="user-name">${user.fullName}</h4>
                    </div>
                </div>
            </div>
        </div>
        <!-- page title area end -->
        <div class="main-content-inner">
            <section>
                <div class="col-lg-12 mt-5">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <div class="float-right mb-5">
                                    <a href="${pageContext.request.contextPath}/course/edit/-1"><button type="button" class="btn btn-success btn-lg" data-toggle="modal"
                                            data-target=".modal-add-new"><i class="fa fa-plus"></i> Nowy
                                    </button></a>
                                </div>

                                <table id="instructorTable" class="table table-striped table-bordered"
                                       style="width:100% !important;">

                                    <thead class="text-uppercase bg-dark" style="width:100% !important;">
                                    <tr class="text-white" style="width:100% !important;">
                                        <th style="width: 3%;">Id</th>
                                        <th style="width: 20%;">Numer kursu</th>
                                        <th style="width: 20%;">Kategoria</th>
                                        <th style="width: 20%;">Data rozpoczęcia</th>
                                        <th style="width: 20%;">Data zakończenia</th>
                                        <th class="text-center" style="width: 27%;">
                                            <div>
                                                <em class="fa fa-cog"></em>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="course" items="${course}">
                                        <tr>
                                            <td>${course.id}</td>
                                            <td>${course.courseNumber}</td>
                                            <td>${course.category.name}</td>
                                            <td>${course.startDate}</td>
                                            <td>${course.endDate}</td>
                                            <td>
                                                <div class="float-right">
                                                    <a class='btn btn-sm btn-warning'
                                                       href="${pageContext.request.contextPath}/course/edit/${course.id}">
                                                        <span class="fa fa-edit"></span> Edytuj</a>
                                                    <button type="button" class="btn btn-danger btn-sm"
                                                            data-toggle="modal"
                                                            data-target=".modal-delete-${course.id}"><span
                                                            class="fa fa-trash"></span> Usuń
                                                    </button>
                                                    <div class="modal fade modal-delete-${course.id}">
                                                        <div class="modal-dialog modal-sm">
                                                            <div class="modal-content">
                                                                <div class="modal-body">
                                                                    Czy napewno chcesz usunąć <b>${course.courseNumber} (Kat. ${course.category.name})</b>?
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary"
                                                                            data-dismiss="modal">
                                                                        Zamknij
                                                                    </button>
                                                                    <a href="${pageContext.request.contextPath}/course/delete/${course.id}"
                                                                       class="btn btn-danger"><span
                                                                            class="fa fa-trash"></span> Usuń</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- main content area end -->
    <!-- footer area start-->
    <footer>
        <div class="footer-area">
            <p>© Copyright 2018. All right reserved. Template by <a href="https://colorlib.com/wp/">Colorlib</a>.</p>
        </div>
    </footer>
    <!-- footer area end-->
</div>
<!-- page container area end -->
<%@include file="static/footer.jsp"%>
</body>

<script type="application/javascript">
    $(document).ready(function () {
        $('#instructorTable').DataTable({
            "language": {
                "decimal": "",
                "emptyTable": "Brak danych w tabeli",
                "info": "Rekordy _START_ do _END_ z _TOTAL_",
                "infoEmpty": "Rekordy 0 do 0 z 0 ",
                "infoFiltered": "(filtered from _MAX_ total entries)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Pokaż _MENU_ rekordów",
                "loadingRecords": "Ładowanie...",
                "processing": "Przetwarzanie...",
                "search": "Wyszukaj:",
                "zeroRecords": "Nie znaleziono rekordów",
                "paginate": {
                    "first": "Pierwszy",
                    "last": "Ostatni",
                    "next": "Następny",
                    "previous": "Poprzedni"
                },
                "aria": {
                    "sortAscending": ": sortuj rosnąco",
                    "sortDescending": ": sortuj malejąco"
                }
            }
        });
    });
</script>

</html>