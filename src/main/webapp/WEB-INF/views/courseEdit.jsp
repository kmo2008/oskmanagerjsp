<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>${course.courseNumber} (${course.id}) - OSKManager</title>
    <%@include file="static/heads.jsp" %>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bootstrap-select.css">
</head>

<body>
<!-- preloader area start -->
<div id="preloader">
    <div class="loader"></div>
</div>
<!-- preloader area end -->
<!-- page container area start -->
<div class="page-container">
    <%@include file="static/menu.jsp" %>
    <!-- main content area start -->
    <div class="main-content">
        <!-- header area start -->
        <div class="header-area">
            <div class="row align-items-center">
                <!-- nav and search button -->
                <div class="col-md-6 col-sm-8 clearfix">
                    <div class="nav-btn pull-left">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <!-- profile info & task notification -->
            </div>
        </div>
        <!-- header area end -->
        <!-- page title area start -->
        <div class="page-title-area">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="breadcrumbs-area clearfix">
                        <h4 class="page-title pull-left">${course.courseNumber}</h4>
                        <ul class="breadcrumbs pull-left">
                            <li><a href="${pageContext.request.contextPath}/dashboard">Ewidencja</a></li>
                            <li><a href="${pageContext.request.contextPath}/client/list">Kursy</a></li>
                            <li>${course.courseNumber}</li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 clearfix">
                    <div class="user-profile pull-right">
                        <%@ page import="pl.kmo2008.oskmanager.entity.enums.Sex" %>
                        <c:choose>
                            <c:when test="${user.sex == Sex.MAN}">
                                <img class="avatar user-thumb"
                                     src="${pageContext.request.contextPath}/static/images/author/avatar_man.png"
                                     alt="avatar">
                            </c:when>
                            <c:otherwise>
                                <img class="avatar user-thumb"
                                     src="${pageContext.request.contextPath}/static/images/author/avatar_woman.png"
                                     alt="avatar">
                            </c:otherwise>
                        </c:choose>
                        <h4 class="user-name">${user.fullName}</h4>
                    </div>
                </div>
            </div>
        </div>
        <!-- page title area end -->
        <div class="main-content-inner">
            <section>
                <div class="mt-5">
                    <div class="card">
                        <div class="card-body ml-4">
                            <div class="container-fluid">
                                <form action="${pageContext.request.contextPath}/course/edit/${course.id}"
                                      method="post">
                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                    <div class="row">
                                        <div class="col-auto ml-auto">
                                            <button type="submit" class="btn btn-success">Zapisz</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <%--3 row--%>
                                        <div class="col-4 mb-3">
                                            <label for="courseNumber">Oznaczenie kursu</label>
                                            <input type="text" class="form-control input-rounded" id="courseNumber"
                                                   value="${course.courseNumber}"
                                                   name="courseNumber"
                                                   required="">
                                            <div class="invalid-feedback">
                                                Wypełnij to pole.
                                            </div>
                                        </div>
                                        <div class="col-4 mb-3">
                                            <label for="startDate">Rozpoczęcie kursu</label>
                                            <input type="date" class="form-control input-rounded" id="startDate"
                                                   name="startDate" value="${course.startDate}" required>

                                        </div>
                                        <div class="col-4 mb-3">

                                            <label for="endDate">Zakończenie kursu</label>
                                            <input type="date" class="form-control input-rounded" id="endDate"
                                                   name="endDate" value="${course.endDate}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <%--3 row--%>
                                        <div class="col-4 mb-3">
                                            <label for="coursePrice">Cena</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control input-rounded" id="coursePrice"
                                                       value="${course.price}"
                                                       name="coursePrice"
                                                       required="">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">zł</span>
                                                </div>
                                                <div class="valid-feedback">
                                                    Ok!
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4 mb-3">
                                        </div>
                                        <div class="col-4 mb-3">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 mb-3">
                                            <b class="text-muted mb-3 mt-4 d-block">Kategorie:</b>
                                            <c:forEach items="${categories}" var="category">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="${category.name}" name="category"
                                                           value="${category.name}"
                                                           <c:if test="${course.category.name == category.name}">checked</c:if>
                                                           class="custom-control-input">
                                                    <label class="custom-control-label"
                                                           for="${category.name}">${category.name}</label>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                </form>
                                <div class="row">
                                    <div class="col-lg-12 mt-5">
                                        <div class="card">
                                            <div class="card-body">
                                                <h4 class="header-title">Kursanci przypisani do kursu</h4>
                                                <div class="single-table">
                                                    <div class="table-responsive">
                                                        <table class="table text-center" id="pkkTable">
                                                            <thead class="text-uppercase bg-dark">
                                                            <tr class="text-white">
                                                                <th scope="col">Imię i nazwisko kursanta</th>
                                                                <th scope="col">Numer PKK</th>
                                                                <th scope="col">Przypisany instruktor</th>
                                                                <th scope="col">Akcje &nbsp;
                                                                    <button type="button"
                                                                            class="btn btn-success btn-xs btn-rounded"
                                                                            data-toggle="modal"
                                                                            data-target=".modal-add-pkk">+
                                                                    </button>
                                                                    <div class="modal fade modal-add-pkk">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content text-left">
                                                                                <input type="hidden"
                                                                                       name="${_csrf.parameterName}"
                                                                                       value="${_csrf.token}"/>
                                                                                <div class="modal-header">
                                                                                    <h4 class="header-title">Dodaj
                                                                                        kursanta do kursu</h4>
                                                                                </div>
                                                                                <form action="${pageContext.request.contextPath}/course/addClient/${course.id}"
                                                                                      method="post">
                                                                                    <input type="hidden"
                                                                                           name="${_csrf.parameterName}"
                                                                                           value="${_csrf.token}"/>
                                                                                    <div class="modal-body">
                                                                                        <select class="selectpicker"
                                                                                                name="newClient"
                                                                                                id="newClient"
                                                                                                data-live-search="true"
                                                                                                data-max-options="5"
                                                                                                data-width="100%">
                                                                                            <c:forEach
                                                                                                    items="${clients}"
                                                                                                    var="client">
                                                                                                <c:forEach
                                                                                                        items="${client.pkk}"
                                                                                                        var="pkk">
                                                                                                    <option value="${client.id}" data-subtext="(Kat.
                                                                                                    <c:forEach
                                                                                                            items="${pkk.categories}"
                                                                                                            var="pkkCat">
                                                                                                        ${pkkCat.name}
                                                                                                    </c:forEach>
                                                                                                    [ ${pkk.number} ] )">${client.fullName}
                                                                                                </c:forEach></option>
                                                                                            </c:forEach>
                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="button"
                                                                                                class="btn btn-secondary"
                                                                                                data-dismiss="modal">
                                                                                            Zamknij
                                                                                        </button>
                                                                                        <button type="submit"
                                                                                                class="btn btn-primary">
                                                                                            Zapisz
                                                                                        </button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <c:forEach var="client" items="${course.clients}">
                                                                <tr class="table-secondary">
                                                                    <td>${client.fullName}</td>
                                                                    <td><c:forEach items="${client.pkk}" var="pkk">
                                                                        <c:forEach items="${pkk.categories}"
                                                                                   var="pkkCat">
                                                                            <c:if test="${pkkCat.name == course.category.name}">
                                                                                ${pkk.number}
                                                                            </c:if>
                                                                        </c:forEach>

                                                                    </c:forEach></td>
                                                                    <td>
                                                                        <c:forEach items="${clientInstructors}"
                                                                                   var="clientInstructor">
                                                                            <c:if test="${client == clientInstructor.client}">
                                                                                ${clientInstructor.instructor.fullName} (${clientInstructor.instructor.instructorNumber})
                                                                            </c:if>
                                                                        </c:forEach>
                                                                    </td>
                                                                    <td>
                                                                        <button type="button"
                                                                                class="btn btn-primary"
                                                                                data-toggle="modal"
                                                                                data-target=".modal-add-${client.id}">
                                                                            Przypisz instruktora
                                                                        </button>
                                                                        <div class="modal fade modal-add-${client.id}">
                                                                            <div class="modal-dialog">
                                                                                <div class="modal-content text-left">
                                                                                    <input type="hidden"
                                                                                           name="${_csrf.parameterName}"
                                                                                           value="${_csrf.token}"/>
                                                                                    <div class="modal-header">
                                                                                        <h4 class="header-title">
                                                                                            Przypisz instruktora</h4>
                                                                                    </div>
                                                                                    <form action="${pageContext.request.contextPath}/client/addInstructor/${course.id}/${client.id}"
                                                                                          method="post">
                                                                                        <input type="hidden"
                                                                                               name="${_csrf.parameterName}"
                                                                                               value="${_csrf.token}"/>
                                                                                        <div class="modal-body">
                                                                                            <select class="selectpicker"
                                                                                                    name="instructor"
                                                                                                    id="instructor"
                                                                                                    data-live-search="true"
                                                                                                    data-max-options="5"
                                                                                                    data-width="100%">
                                                                                                <c:forEach
                                                                                                        items="${instructors}"
                                                                                                        var="instructor">
                                                                                                    <option value="${instructor.id}"
                                                                                                            data-subtext="(Kat.
                                                                                                    <c:forEach items="${instructor.categories}" var="cat">
                                                                                                        ${cat.name},
                                                                                                    </c:forEach>
                                                                                                     )">${instructor.fullName}
                                                                                                    </option>
                                                                                                </c:forEach>
                                                                                            </select>
                                                                                        </div>
                                                                                        <div class="modal-footer">
                                                                                            <button type="button"
                                                                                                    class="btn btn-secondary"
                                                                                                    data-dismiss="modal">
                                                                                                Zamknij
                                                                                            </button>
                                                                                            <button type="submit"
                                                                                                    class="btn btn-primary">
                                                                                                Zapisz
                                                                                            </button>
                                                                                        </div>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <a href="${pageContext.request.contextPath}/course/deleteClient/${client.id}/${course.id}">
                                                                            <button type="button"
                                                                                    class="btn btn-danger"><span
                                                                                    class="fa fa-trash"></span> Usuń
                                                                                kursanta
                                                                            </button>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </c:forEach>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>


    <!-- main content area end -->
    <!-- footer area start-->
    <footer>
        <div class="footer-area">
            <p>© Copyright 2018. All right reserved. Template by <a href="https://colorlib.com/wp/">Colorlib</a>.</p>
        </div>
    </footer>
</div>
<!-- footer area end-->
<!-- page container area end -->
<%@include file="static/footer.jsp" %>

</body>


<script src="${pageContext.request.contextPath}/static/js/bootstrap-select.js"></script>

<script type="application/javascript">

    $(document).ready(function () {
        $('#newClient').selectpicker();

        $('#pkkTable').DataTable({
            "language": {
                "decimal": "",
                "emptyTable": "Brak danych w tabeli",
                "info": "Rekordy _START_ do _END_ z _TOTAL_",
                "infoEmpty": "Rekordy 0 do 0 z 0 ",
                "infoFiltered": "(filtered from _MAX_ total entries)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Pokaż _MENU_ rekordów",
                "loadingRecords": "Ładowanie...",
                "processing": "Przetwarzanie...",
                "search": "Wyszukaj:",
                "zeroRecords": "Nie znaleziono rekordów",
                "paginate": {
                    "first": "Pierwszy",
                    "last": "Ostatni",
                    "next": "Następny",
                    "previous": "Poprzedni"
                },
                "aria": {
                    "sortAscending": ": sortuj rosnąco",
                    "sortDescending": ": sortuj malejąco"
                }
            }
        });
    });

</script>

</html>