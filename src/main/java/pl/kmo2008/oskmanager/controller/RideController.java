package pl.kmo2008.oskmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.kmo2008.oskmanager.entity.Ride;
import pl.kmo2008.oskmanager.entity.enums.RideType;
import pl.kmo2008.oskmanager.entity.extensions.Instructor;
import pl.kmo2008.oskmanager.service.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.List;

@Controller
public class RideController {

    @Autowired
    ModelService modelService;

    @Autowired
    RideService rideService;

    @Autowired
    InstructorService instructorService;

    @Autowired
    ClientService clientService;

    @Autowired
    CarService carService;

    @Autowired
    CourseService courseService;


    @GetMapping(path = "/ride/list")
    public ModelAndView home(){
        ModelAndView modelAndView = modelService.getNew();
        if(modelService.isInstrcutor()){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            Instructor instructor = instructorService.findInstructorByEmail(auth.getName());
            List<Ride> rides = rideService.findAllByInstructorId(instructor.getId());
            final double[] payment = {0.0};
            rides.forEach(ride -> {
                double price = 0;
                if(ride.getRideType() == RideType.NORMAL) price = instructor.getNormalPayment();
                else price = instructor.getAdditionalPayment();
                if(ride.getDate().getMonthValue() == LocalDate.now().getMonthValue())
                payment[0] += ride.getDuration() * price;
            });
            modelAndView.addObject("payment", payment[0]);
            modelAndView.addObject("rides", rides);
            modelAndView.addObject("user", instructor);
            modelAndView.addObject("instr", true);
        }
        else {
            modelAndView.addObject("rides", rideService.findAll());
        }
        modelAndView.addObject("today", LocalDate.now());
        modelAndView.addObject("clients", clientService.findAll());
        modelAndView.addObject("cars", carService.findAll());
        modelAndView.addObject("courses", courseService.findAll());
        modelAndView.addObject("instructors", instructorService.findAll());
        modelAndView.setViewName("rides");
        return modelAndView;
    }


    @GetMapping(path = "/ride/list/{back}/{forward}/{date}")
    public ModelAndView home(@PathVariable int back, @PathVariable int forward, @PathVariable String date){
        ModelAndView modelAndView = modelService.getNew();
        LocalDate dateLocal = LocalDate.parse(date);
        if(modelService.isInstrcutor()){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            Instructor instructor = instructorService.findInstructorByEmail(auth.getName());
            List<Ride> rides = rideService.findAllByInstructorId(instructor.getId());
            final double[] payment = {0.0};
            LocalDate finalDateLocal = dateLocal;
            rides.forEach(ride -> {
                double price = 0;
                if(ride.getRideType() == RideType.NORMAL) price = instructor.getNormalPayment();
                else price = instructor.getAdditionalPayment();
                if(finalDateLocal.getMonthValue() == LocalDate.now().getMonthValue())
                    payment[0] += ride.getDuration() * price;
            });
            modelAndView.addObject("payment", payment[0]);
            modelAndView.addObject("rides", rides);
            modelAndView.addObject("user", instructor);
            modelAndView.addObject("instr", true);
        }
        else {
            modelAndView.addObject("rides", rideService.findAll());
        }
        if(back == 1)
            dateLocal = dateLocal.minusDays(1);
        else if(forward == 1)
            dateLocal = dateLocal.plusDays(1);
        modelAndView.addObject("today", dateLocal);
        modelAndView.addObject("clients", clientService.findAll());
        modelAndView.addObject("cars", carService.findAll());
        modelAndView.addObject("courses", courseService.findAll());
        modelAndView.addObject("instructors", instructorService.findAll());
        modelAndView.setViewName("rides");
        return modelAndView;
    }

    @GetMapping(path = "/ride/delete/{id}/{date}")
    public String delete(@PathVariable long id, @PathVariable String date){
        rideService.delete(id);
        return "redirect:/ride/list/0/0/" + date ;
    }

    @PostMapping(path = "ride/add/{date}")
    public String add(@PathVariable String date, HttpServletRequest request){
        rideService.add(request, date, modelService.isInstrcutor());
        return "redirect:/ride/list/0/0/" + date ;
    }
}
