package pl.kmo2008.oskmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.kmo2008.oskmanager.entity.Course;
import pl.kmo2008.oskmanager.repository.ClientInstructorRepostiory;
import pl.kmo2008.oskmanager.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class CourseController {

    @Autowired
    ModelService modelService;

    @Autowired
    CourseService courseService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    ClientService clientService;

    @Autowired
    InstructorService instructorService;

    @Autowired
    ClientInstructorRepostiory clientInstructorRepostiory;

    @GetMapping(path = "/course/list")
    public ModelAndView home() {
        ModelAndView modelAndView = modelService.getNew();
        modelAndView.addObject("course", courseService.findAll());
        modelAndView.setViewName("course");
        return modelAndView;
    }

    @GetMapping(path = "/course/edit/{id}")
    public ModelAndView instructorEdit(@PathVariable("id") Integer id) {
        ModelAndView modelAndView = modelService.getNew();
        Course course;
        if (id == -1) {
            course = new Course();
            courseService.save(course);
        } else course = courseService.findById(id);
        modelAndView.addObject("course", course);
        modelAndView.addObject("categories", categoryService.findAll());
        modelAndView.addObject("clients", clientService.findAll());
        modelAndView.addObject("instructors", instructorService.findAll());
        modelAndView.addObject("clientInstructors", clientInstructorRepostiory.findAllByCourse(course));
        modelAndView.setViewName("courseEdit");
        return modelAndView;
    }

    @PostMapping(path = "/course/edit/{id}")
    public String courseEdit(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        courseService.edit(request, id);
        return "redirect:/course/edit/" + id;
    }

    @PostMapping(path = "/course/addClient/{id}")
    public String courseAddClient(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        courseService.addClient(request, id);
        return "redirect:/course/edit/" + id;
    }

    @GetMapping(path = "/course/delete/{id}")
    public String delete(@PathVariable("id") Integer id) {
        courseService.delete(id);
        return "redirect:/course/list";
    }


}
