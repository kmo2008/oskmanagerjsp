package pl.kmo2008.oskmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.kmo2008.oskmanager.entity.enums.AlertType;
import pl.kmo2008.oskmanager.service.AlertService;
import pl.kmo2008.oskmanager.service.ModelService;
import pl.kmo2008.oskmanager.service.UserService;

import javax.servlet.http.HttpServletRequest;

@Controller
public class DashboardController {
    @Autowired
    ModelService modelService;

    @Autowired
    AlertService alertService;

    @Autowired
    UserService userService;

    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public ModelAndView dashboard() {
        ModelAndView modelAndView = modelService.getNew();
        modelAndView.addObject("ReviewAlert", alertService.findFirstTenAlerts(AlertType.REVIEW));
        modelAndView.addObject("InsuranceAlert", alertService.findFirstTenAlerts(AlertType.INSURANCE));
        modelAndView.setViewName("index");
        return modelAndView;
    }

    @GetMapping(path = "/user/changePassword")
    public ModelAndView changePassword() {
        ModelAndView modelAndView = modelService.getNew();
        modelAndView.setViewName("changePassword");
        return modelAndView;
    }

    @PostMapping(path = "/user/changePassword")
    public String changePassword(HttpServletRequest request){
        return "redirect:/user/changePassword/" + userService.changePassword(request);
    }

    @GetMapping(path = "/user/changePassword/{result}")
    public ModelAndView changePassword(HttpServletRequest request, @PathVariable String result){
        ModelAndView modelAndView = modelService.getNew();
        modelAndView.addObject("result", result);
        modelAndView.setViewName("changePassword");
        return modelAndView;
    }
}
