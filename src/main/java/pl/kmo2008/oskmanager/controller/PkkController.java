package pl.kmo2008.oskmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import pl.kmo2008.oskmanager.service.PKKService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class PkkController {

    @Autowired
    PKKService pkkService;

    @PostMapping(path = "/pkk/add/{id}")
    public String add(@PathVariable("id") Integer clientId, HttpServletRequest request, HttpServletResponse response){
        pkkService.add(clientId, request);
        return "redirect:/client/edit/" + clientId;
    }

    @GetMapping(path = "/pkk/delete/{id}/{clientId}")
    public String delete(@PathVariable("id") Integer id, @PathVariable("clientId") Integer clientId){
        pkkService.delete(id);
        return "redirect:/client/edit/" + clientId;
    }
}
