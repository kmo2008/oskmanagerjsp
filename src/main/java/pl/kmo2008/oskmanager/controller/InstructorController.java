package pl.kmo2008.oskmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.kmo2008.oskmanager.entity.Ride;
import pl.kmo2008.oskmanager.entity.enums.RideType;
import pl.kmo2008.oskmanager.entity.extensions.Instructor;
import pl.kmo2008.oskmanager.service.CategoryService;
import pl.kmo2008.oskmanager.service.InstructorService;
import pl.kmo2008.oskmanager.service.ModelService;
import pl.kmo2008.oskmanager.service.RideService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class InstructorController {

    @Autowired
    ModelService modelService;

    @Autowired
    InstructorService instructorService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    RideService rideService;

    @RequestMapping(path = "/instructor/list", method = RequestMethod.GET)
    public ModelAndView instructorList() {
        ModelAndView modelAndView = modelService.getNew();
        modelAndView.addObject("instructors", instructorService.findAll());
        modelAndView.addObject("categories", categoryService.findAll());
        modelAndView.setViewName("instructor");
        return modelAndView;
    }

    @PostMapping(path = "/instructor/add")
    public String instructorAdd(HttpServletRequest request, HttpServletResponse response) {
        instructorService.add(request);
        return "redirect:/instructor/list";
    }

    @GetMapping(path = "/instructor/edit/{id}")
    public ModelAndView instructorEdit(@PathVariable("id") Integer id) {
        ModelAndView modelAndView = modelService.getNew();
        modelAndView.addObject("instructor", instructorService.findById(id));
        modelAndView.addObject("categories", categoryService.findAll());

        List<Ride> rides = rideService.findAllByInstructorId(id).stream().sorted(Comparator.comparing(Ride::getDate)).collect(Collectors.toList());
        List<Ride> rider = rides.stream()
                .filter(ride ->
                        ride.getDate().getMonthValue() == LocalDate.now().getMonthValue() || ride.getDate().getMonthValue() == LocalDate.now().minusMonths(1).getMonthValue() || ride.getDate().getMonthValue() == LocalDate.now().minusMonths(2).getMonthValue()
                ).collect(Collectors.toList());

        HashMap<Integer, Double> payment = new HashMap<>();


        rider.forEach(ride -> {
            payment.put(ride.getMotnh(), getPayment(ride.getMotnh(), ride.getDuration(), ride.getRideType(),instructorService.findById(id), payment));
        });



        modelAndView.addObject("rides", payment);
        modelAndView.setViewName("instructorEdit");
        return modelAndView;
    }

    @PostMapping(path = "/instructor/edit/{id}/name")
    public String instructorEditName(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        instructorService.editName(request, id);
        return "redirect:/instructor/edit/" + id;
    }

    @PostMapping(path = "/instructor/edit/{id}/all")
    public String instructorEditAll(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        instructorService.edit(request, id);
        return "redirect:/instructor/edit/" + id;
    }

    @GetMapping(path = "/instructor/delete/{id}")
    public String instructorDelete(@PathVariable("id") Integer id) {
        instructorService.delete(id);
        return "redirect:/instructor/list";
    }

    @GetMapping(path = "/instructor/active/{id}")
    public String active(@PathVariable("id") Integer id) {
        instructorService.active(instructorService.findById(id));
        return "redirect:/instructor/list";
    }

    private double getPayment(int month, double duration, RideType type, Instructor instructor, HashMap payment){
        double old;
        if(payment.get(month) == null){
            old = 0.0;
        }
        else old = (double) payment.get(month);
        if(type == RideType.NORMAL)
            return instructor.getNormalPayment() * duration + old;
        else return instructor.getAdditionalPayment() * duration + old;
    }
}

