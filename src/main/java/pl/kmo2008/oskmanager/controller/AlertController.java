package pl.kmo2008.oskmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.kmo2008.oskmanager.service.AlertService;

@Controller
@RequestMapping("/alert")
public class AlertController {

    @Autowired
    AlertService alertService;

    @GetMapping(path = "/delete/{id}")
    public String alertDelete(@PathVariable("id") int id) {
        alertService.delete(alertService.find(id));
        return "redirect:/dashboard";
    }

}
