package pl.kmo2008.oskmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.kmo2008.oskmanager.service.CategoryService;
import pl.kmo2008.oskmanager.service.ClientService;
import pl.kmo2008.oskmanager.service.ModelService;
import pl.kmo2008.oskmanager.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class ClientController {

    @Autowired
    UserService userService;

    @Autowired
    ModelService modelService;

    @Autowired
    ClientService clientService;

    @Autowired
    CategoryService categoryService;

    @RequestMapping(value="/client/list", method = RequestMethod.GET)
    public ModelAndView home(){
        ModelAndView modelAndView = modelService.getNew();
        modelAndView.addObject("clients", clientService.findAll());
        modelAndView.setViewName("client");
        return modelAndView;
    }

    @GetMapping(path = "/client/edit/{id}")
    public ModelAndView instructorEdit(@PathVariable("id") Integer id) {
        ModelAndView modelAndView = modelService.getNew();
        modelAndView.addObject("client", clientService.findById(id));
        modelAndView.addObject("categories", categoryService.findAll());
        modelAndView.setViewName("clientEdit");
        return modelAndView;
    }

    @GetMapping(path = "/client/active/{id}")
    public String active(@PathVariable("id") Integer id){
        clientService.active(clientService.findById(id));
        return "redirect:/client/list";
    }

    @PostMapping(path = "/client/add")
    public String clientEdit(HttpServletRequest request, HttpServletResponse response) {
        clientService.add(request);
        return "redirect:/client/list";
    }

    @PostMapping(path = "/client/edit/{id}")
    public String clientEdit(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        clientService.edit(request, id);
        return "redirect:/client/edit/"+id;
    }

    @PostMapping(path = "/client/addInstructor/{courseId}/{clientId}")
    public String clientAddInstructor(@PathVariable Integer courseId, @PathVariable Integer clientId, HttpServletRequest request){
        clientService.addInstructor(courseId, clientId, request);
        return "redirect:/course/edit/" + courseId;
    }

    @GetMapping(path = "/client/delete/{id}")
    public String delete(@PathVariable("id") Integer id){
        clientService.delete(id);
        return "redirect:/client/list";
    }
}
