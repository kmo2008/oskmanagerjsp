package pl.kmo2008.oskmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.kmo2008.oskmanager.service.AlertService;
import pl.kmo2008.oskmanager.service.CarService;
import pl.kmo2008.oskmanager.service.CategoryService;
import pl.kmo2008.oskmanager.service.ModelService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class CarController {

    @Autowired
    ModelService modelService;

    @Autowired
    CarService carService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    AlertService alertService;

    @RequestMapping(path = "/car/list", method = RequestMethod.GET)
    public ModelAndView instructorList() {
        ModelAndView modelAndView = modelService.getNew();
        modelAndView.addObject("cars", carService.findAll());
        modelAndView.addObject("categories", categoryService.findAll());
        modelAndView.setViewName("car");
        return modelAndView;
    }

    @PostMapping(path = "/car/add")
    public String add(HttpServletRequest request, HttpServletResponse response){
        carService.add(request);
        return "redirect:/car/list";
    }

    @PostMapping(path = "/car/edit/{id}")
    public String edit(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response){
        carService.edit(id, request);
        return "redirect:/car/list";
    }

    @GetMapping(path = "/car/delete/{id}")
    public String delete(@PathVariable("id") Integer id){
        alertService.deleteByCarId(id);
        carService.delete(id);
        return "redirect:/car/list";
    }
}
