package pl.kmo2008.oskmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.kmo2008.oskmanager.entity.Transaction;
import pl.kmo2008.oskmanager.entity.extensions.Client;
import pl.kmo2008.oskmanager.service.ClientService;
import pl.kmo2008.oskmanager.service.CourseService;
import pl.kmo2008.oskmanager.service.ModelService;
import pl.kmo2008.oskmanager.service.TransactionService;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;

@Controller
public class TransactionController {

    @Autowired
    ModelService modelService;

    @Autowired
    TransactionService transactionService;

    @Autowired
    ClientService clientService;

    @Autowired
    CourseService courseService;

    @GetMapping(path = "/transaction/add/{clientId}")
    public ModelAndView addTransaction(@PathVariable Integer clientId){
        ModelAndView modelAndView = modelService.getNew();
        Client client = clientService.findById(clientId);
        modelAndView.addObject("client", client);
        modelAndView.addObject("clientCourses", courseService.findByClient(client));
        modelAndView.addObject("clientTransactions", transactionService.findByClient(client));
        modelAndView.setViewName("transactionClient");
        return modelAndView;
    }

    @PostMapping(path = "/transaction/add")
    public String addTrans(HttpServletRequest request){
        int clientId = Integer.parseInt(request.getParameter("clientId"));
        transactionService.add(clientId, request);
        return "redirect:/transaction/add/" + clientId;
    }

    @GetMapping(path = "/transaction/list")
    public ModelAndView home(){
        ModelAndView modelAndView = modelService.getNew();
        modelAndView.addObject("transactions", transactionService.findAll());
        modelAndView.addObject("today", LocalDate.now());
        modelAndView.setViewName("transactions");
        return modelAndView;
    }

    @GetMapping(path = "/transaction/list/{back}/{forward}/{date}")
    public ModelAndView home(@PathVariable int back, @PathVariable int forward, @PathVariable String date){
        ModelAndView modelAndView = modelService.getNew();
        modelAndView.addObject("transactions", transactionService.findAll());
        LocalDate dateLocal = LocalDate.parse(date);
        if(back == 1)
            dateLocal = dateLocal.minusDays(1);
        else if(forward == 1)
            dateLocal = dateLocal.plusDays(1);
        else dateLocal = LocalDate.now();
        modelAndView.addObject("today", dateLocal);
        modelAndView.setViewName("transactions");
        return modelAndView;
    }

    @GetMapping(path = "/transaction/delete/{id}")
    public String delete(@PathVariable long id){
        Transaction transaction = transactionService.findById(id);
        int clientId = transaction.getClient().getId();
        transactionService.delete(transaction);
        return "redirect:/transaction/add/" + clientId;
    }
}
