package pl.kmo2008.oskmanager.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import pl.kmo2008.oskmanager.entity.*;
import pl.kmo2008.oskmanager.entity.enums.Document;
import pl.kmo2008.oskmanager.entity.enums.RideType;
import pl.kmo2008.oskmanager.entity.enums.Sex;
import pl.kmo2008.oskmanager.entity.extensions.Client;
import pl.kmo2008.oskmanager.entity.extensions.Instructor;
import pl.kmo2008.oskmanager.repository.ClientInstructorRepostiory;
import pl.kmo2008.oskmanager.service.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

@Component
public class StartUpConfiguration implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    RoleService roleService;

    @Autowired
    UserService userService;

    @Autowired
    AlertService alertService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    CarService carService;

    @Autowired
    PKKService pkkService;

    @Autowired
    CourseService courseService;

    @Autowired
    ClientService clientService;

    @Autowired
    TransactionService transactionService;

    @Autowired
    ClientInstructorRepostiory clientInstructorRepostiory;

    @Autowired
    InstructorService instructorService;

    @Autowired
    RideService rideService;

    @Override
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        checkRoles();
        checkCategories();
        firstUser();
        addSimpleAlerts();
        addSimpleCars();
        addSimpleCourse();
        addClientInstructor();
        addSimpleTrans();
        addSimpleRides();
        System.out.println("Zakończono wypełnianie przykłądowymi danymi");
    }

    private void addSimpleRides(){
        Ride ride = new Ride();
        Client client = clientService.findClientByEmail("client@client.pl");
        Course course = courseService.finByCourseNumber("01/2019");
        Instructor instructor = instructorService.findInstructorByEmail("ins@ins.pl");
        Car car = carService.findByPlate("GTC 10713");
        ride.setCar(car);
        ride.setClient(client);
        ride.setCourse(course);
        ride.setInstructor(instructor);
        ride.setRideType(RideType.NORMAL);
        ride.setDate(LocalDate.now());
        ride.setDuration(2.0);
        rideService.save(ride);


        ride = new Ride();
        ride.setCar(car);
        ride.setClient(client);
        ride.setCourse(course);
        ride.setInstructor(instructor);
        ride.setRideType(RideType.NORMAL);
        ride.setDate(LocalDate.now().minusMonths(2));
        ride.setDuration(2.0);
        rideService.save(ride);

        ride = new Ride();
        ride.setCar(car);
        ride.setClient(client);
        ride.setCourse(course);
        ride.setInstructor(instructor);
        ride.setRideType(RideType.ADDITIONAL);
        ride.setDate(LocalDate.now().minusMonths(4));
        ride.setDuration(2.0);
        rideService.save(ride);
    }

    private void addClientInstructor(){
        Client client = clientService.findClientByEmail("client@client.pl");
        Course course = courseService.finByCourseNumber("01/2019");
        if(!clientInstructorRepostiory.findByClientIdAndCourseId(client.getId(), course.getId()).isPresent()){
            ClientInstructor clientInstructor = new ClientInstructor();
            clientInstructor.setInstructor(instructorService.findInstructorByEmail("ins@ins.pl"));
            clientInstructor.setCourse(course);
            clientInstructor.setClient(client);
            clientInstructorRepostiory.save(clientInstructor);
        }

    }

    private void addSimpleTrans(){
        if(transactionService.findByClient(clientService.findClientByEmail("client@client.pl")).isEmpty()){
            Client client = clientService.findClientByEmail("client@client.pl");
            Course course = courseService.finByCourseNumber("01/2019");
            Transaction transaction = new Transaction();
            transaction.setClient(client);
            transaction.setCourse(course);
            transaction.setDate(LocalDate.now());
            transaction.setValue(500.0);
            transactionService.save(transaction);
        }
    }
    private void addSimpleCourse(){
        System.out.println("--Wgrywanie kursów--");
        if(courseService.finByCourseNumber("01/2019") == null){
            Course course = new Course();
            course.setCategory(categoryService.findByName("B"));
            Client client = clientService.findClientByEmail("client@client.pl");
            ArrayList<Client> clients = new ArrayList<>();
            clients.add(client);
            course.setClients(clients);
            course.setStartDate(LocalDate.now().minusDays(20));
            course.setPrice(1500);
            course.setCourseNumber("01/2019");
            courseService.save(course);
            System.out.println("Dodano kurs 01/2019");
        }
        System.out.println("--Koniec wgrywania kursów--");
    }

    private void checkCategories() {
        System.out.println("--Wgrywanie kategorii--");
        if (categoryService.findByName("AM") == null) {
            Category cat = new Category();
            cat.setName("AM");
            categoryService.save(cat);
            System.out.println("Kategoria AM");
        }
        if (categoryService.findByName("A1") == null) {
            Category cat = new Category();
            cat.setName("A1");
            categoryService.save(cat);
            System.out.println("Kategoria A1");
        }
        if (categoryService.findByName("A2") == null) {
            Category cat = new Category();
            cat.setName("A2");
            categoryService.save(cat);
            System.out.println("Kategoria A2");
        }
        if (categoryService.findByName("A") == null) {
            Category cat = new Category();
            cat.setName("A");
            categoryService.save(cat);
            System.out.println("Kategoria A");
        }
        if (categoryService.findByName("B1") == null) {
            Category cat = new Category();
            cat.setName("B1");
            categoryService.save(cat);
            System.out.println("Kategoria B1");
        }
        if (categoryService.findByName("B") == null) {
            Category cat = new Category();
            cat.setName("B");
            categoryService.save(cat);
            System.out.println("Kategoria B");
        }
        if (categoryService.findByName("B+E") == null) {
            Category cat = new Category();
            cat.setName("B+E");
            categoryService.save(cat);
            System.out.println("Kategoria B+E");
        }
        if (categoryService.findByName("C1") == null) {
            Category cat = new Category();
            cat.setName("C1");
            categoryService.save(cat);
            System.out.println("Kategoria C1");
        }
        if (categoryService.findByName("C1+E") == null) {
            Category cat = new Category();
            cat.setName("C1+E");
            categoryService.save(cat);
            System.out.println("Kategoria C1+E");
        }
        if (categoryService.findByName("C") == null) {
            Category cat = new Category();
            cat.setName("C");
            categoryService.save(cat);
            System.out.println("Kategoria C");
        }
        if (categoryService.findByName("C+E") == null) {
            Category cat = new Category();
            cat.setName("C+E");
            categoryService.save(cat);
            System.out.println("Kategoria C+E");
        }
        if (categoryService.findByName("D1") == null) {
            Category cat = new Category();
            cat.setName("D1");
            categoryService.save(cat);
            System.out.println("Kategoria D1");
        }
        if (categoryService.findByName("D1+E") == null) {
            Category cat = new Category();
            cat.setName("D1+E");
            categoryService.save(cat);
            System.out.println("Kategoria D1+E");
        }
        if (categoryService.findByName("D") == null) {
            Category cat = new Category();
            cat.setName("D");
            categoryService.save(cat);
            System.out.println("Kategoria D");
        }
        if (categoryService.findByName("D+E") == null) {
            Category cat = new Category();
            cat.setName("D+E");
            categoryService.save(cat);
            System.out.println("Kategoria D+E");
        }
        if (categoryService.findByName("T") == null) {
            Category cat = new Category();
            cat.setName("T");
            categoryService.save(cat);
            System.out.println("Kategoria T");
        }
        System.out.println("--Koniec wgrywania kategorii--");
    }

    private void addSimpleCars(){
        System.out.println("--Wgrywanie pojazdów--");
        if(carService.findByPlate("GTC 10713") == null){
            Car car = new Car();
            car.setMark("KIA");
            car.setModel("RIO");
            car.setPlate("GTC 10713");
            car.setProductionYear(2017);
            car.setEndOfInsurance(LocalDate.now().plusMonths(1));
            car.setEndOfReview(LocalDate.now().plusDays(3));
            HashSet<Category> cats = new HashSet<>();
            cats.add(categoryService.findByName("B"));
            car.setCategories(cats);
            carService.save(car);
            System.out.println("Dodano pojazd GTC 10713");
        }
        System.out.println("--Koniec wgrywania pojazdów--");
    }

    private void firstUser() {
        System.out.println("--Wgrywanie użytkowników");
        if (userService.findUserByEmail("admin@admin.pl") == null) {
            User user = new User();
            user.setEmail("admin@admin.pl");
            user.setPassword("admin");
            user.setActive(1);
            user.setName("Kamil");
            user.setLastName("Ostrowski");
            user.setApartmentNumber("3");
            user.setHouseNumber("6A");
            user.setStreet("Jana Brzechwy");
            user.setPostCode("83-110");
            user.setCity("Tczew");
            user.setPhone("669119411");
            user.setSecondName("Maciej");
            user.setPesel("95060430345");
            user.setDocument(Document.ID);
            user.setDocumentNumber("SDV 345876");
            user.setSex(Sex.MAN);
            HashSet<Role> roles = new HashSet<>();
            roles.add(roleService.find("ADMIN"));
            roles.add(roleService.find("BOSS"));
            roles.add(roleService.find("OFFICE"));
            user.setRoles(roles);
            userService.saveUser(user);
            System.out.println("Dodano konto admina");
        }
        if (userService.findUserByEmail("client@client.pl") == null) {
            Client user = new Client();
            user.setEmail("client@client.pl");
            user.setActive(1);
            user.setName("Monika");
            user.setLastName("Kowalska");
            user.setSex(Sex.WOMAN);
            user.setPesel("56855412552");
            user.setCity("Tczew");
            user.setApartmentNumber("12");
            user.setHouseNumber("5C");
            user.setStreet("Jabłowniowa");
            user.setPostCode("83-110");
            user.setDocument(Document.ID);
            user.setDocumentNumber("AXD 464857");
            user.setSecondName("Anna");
            user.setPhone("600365521");
            HashSet<Role> roles = new HashSet<>();
            roles.add(roleService.find("CLIENT"));
            user.setRoles(roles);
            PKK pkk = new PKK();
            pkk.setNumber("xxxxx xxxxx xxxxx xxxxx");
            pkk.setAddDate(LocalDate.now().minusDays(3));
            HashSet<Category> cats = new HashSet<>();
            cats.add(categoryService.findByName("C"));
            cats.add(categoryService.findByName("B"));
            pkk.setCategories(cats);
            pkkService.save(pkk);
            user.setPkk(new HashSet<>(Collections.singletonList(pkk)));
            userService.saveUser(user);
            System.out.println("Dodano konto klienta");
        }
        if (userService.findUserByEmail("ins@ins.pl") == null) {
            Instructor user = new Instructor();
            user.setEmail("ins@ins.pl");
            user.setPassword("ins");
            user.setActive(0);
            user.setName("Jan");
            user.setSecondName("Marian");
            user.setLastName("Kowalski");
            user.setSex(Sex.MAN);
            user.setInstructorNumber("GTC 0107");
            user.setPhone("669119411");
            user.setApartmentNumber("3");
            user.setHouseNumber("12C");
            user.setPostCode("83-110");
            user.setStreet("Jagielońska");
            user.setDocument(Document.ID);
            user.setDocumentNumber("FGV 456789");
            user.setNormalPayment(14.0);
            user.setAdditionalPayment(20.0);
            user.setCity("Tczew");
            user.setPesel("75485738523");
            Role userRole = roleService.find("INSTRUCTOR");
            HashSet<Category> cats = new HashSet<>();
            cats.add(categoryService.findByName("C"));
            cats.add(categoryService.findByName("B"));
            user.setCategories(cats);
            user.setRoles(new HashSet<>(Collections.singletonList(userRole)));
            userService.saveUser(user);
            System.out.println("Dodano przykładowego instruktora");
        }

    }


    private void checkRoles() {
        System.out.println("--Dodawanie ról--");
        if (!roleService.isRoleWithNameExits("ADMIN")) {
            System.out.println("Dodano role ADMIN");
            Role role = new Role();
            role.setRole("ADMIN");
            roleService.save(role);
        }
        if (!roleService.isRoleWithNameExits("INSTRUCTOR")) {
            System.out.println("Dodano role INSTRUCTOR");
            Role role = new Role();
            role.setRole("INSTRUCTOR");
            roleService.save(role);
        }
        if (!roleService.isRoleWithNameExits("LECTURER")) {
            System.out.println("Dodano role LECTURER");
            Role role = new Role();
            role.setRole("LECTURER");
            roleService.save(role);
        }
        if (!roleService.isRoleWithNameExits("BOSS")) {
            System.out.println("Dodano role BOSS");
            Role role = new Role();
            role.setRole("BOSS");
            roleService.save(role);
        }
        if (!roleService.isRoleWithNameExits("OFFICE")) {
            System.out.println("Dodano role OFFICE");
            Role role = new Role();
            role.setRole("OFFICE");
            roleService.save(role);
        }
        System.out.println("--Koniec dodawania ról--");
    }


    private void addSimpleAlerts() {
//        System.out.println("--Dodawanie przykładowych alertów--");
//        if (alertService.findByTitle("Badanie techniczne") == null) {
//            Alert alert = new Alert();
//            alert.setTitle("Badanie techniczne");
//            alert.setDate(LocalDateTime.now().plusMonths(3));
//            alert.setType(AlertType.REVIEW);
//            alert.setDescription("Koniec badania technicznego: GTC 10713");
//            alertService.save(alert);
//            System.out.println("Dodano alert badania technicznego");
//        }
//        if (alertService.findByTitle("Ubezpieczenie") == null) {
//            Alert alert = new Alert();
//            alert.setTitle("Ubezpieczenie");
//            alert.setDate(LocalDateTime.now().plusMonths(3));
//            alert.setType(AlertType.INSURANCE);
//            alert.setDescription("Koniec ubezpieczenia: GTC 10713");
//            alertService.save(alert);
//            System.out.println("Dodano alert ubezpieczenia");
//        }
//        System.out.println("--Koniec dodawania alertów--");
    }
}