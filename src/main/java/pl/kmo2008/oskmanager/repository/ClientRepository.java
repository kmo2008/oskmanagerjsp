package pl.kmo2008.oskmanager.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import pl.kmo2008.oskmanager.entity.PKK;
import pl.kmo2008.oskmanager.entity.extensions.Client;

import java.util.Optional;

@Repository
public interface ClientRepository extends PagingAndSortingRepository<Client, Integer> {
    Optional<Client> findByPkk(PKK pkk);
    Optional<Client> findByEmail(String email);
}
