package pl.kmo2008.oskmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kmo2008.oskmanager.entity.PKK;

@Repository
public interface PKKRepository extends JpaRepository<PKK, Integer> {
}
