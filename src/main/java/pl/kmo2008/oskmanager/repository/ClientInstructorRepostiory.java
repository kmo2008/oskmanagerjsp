package pl.kmo2008.oskmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kmo2008.oskmanager.entity.ClientInstructor;
import pl.kmo2008.oskmanager.entity.Course;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientInstructorRepostiory extends JpaRepository<ClientInstructor, Integer> {
    Optional<ClientInstructor> findByClientIdAndCourseId(int client, int course);
    List<ClientInstructor> findAllByCourse(Course course);
}
