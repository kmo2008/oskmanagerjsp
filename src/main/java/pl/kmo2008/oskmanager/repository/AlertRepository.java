package pl.kmo2008.oskmanager.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import pl.kmo2008.oskmanager.entity.Alert;
import pl.kmo2008.oskmanager.entity.enums.AlertType;

import java.util.List;
import java.util.Optional;

@Repository
public interface AlertRepository extends PagingAndSortingRepository<Alert, Integer> {

    Alert findFirstByTitle(String title);

    Page<Alert> findAllByType(AlertType type, Pageable pageable);

    Optional<Alert> findByCarAndType(Integer id, AlertType type);

    List<Alert> findByCar(Integer id);

}
