package pl.kmo2008.oskmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.kmo2008.oskmanager.entity.Course;
import pl.kmo2008.oskmanager.entity.extensions.Client;

import java.util.Optional;

@Repository
public interface CourseRepository extends JpaRepository<Course, Integer> {
    Optional<Course> findByCourseNumber(String courseNumber);
    Optional<Course> findByClientsContaining(Client client);
}
