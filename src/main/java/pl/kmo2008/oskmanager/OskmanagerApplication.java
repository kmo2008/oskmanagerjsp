package pl.kmo2008.oskmanager;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class OskmanagerApplication{

    public static void main(String[] args) {
        SpringApplication.run(OskmanagerApplication.class, args);
    }
}

