package pl.kmo2008.oskmanager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.kmo2008.oskmanager.entity.Alert;
import pl.kmo2008.oskmanager.entity.enums.AlertType;
import pl.kmo2008.oskmanager.repository.AlertRepository;

import java.time.LocalDate;
import java.util.List;

@Service
public class AlertService {

    @Autowired
    AlertRepository alertRepository;


    public void save(Alert alert) {
        if (alertRepository.findByCarAndType(alert.getCar(), alert.getType()).isPresent()) this.update(alert);
        else alertRepository.save(alert);
    }


    public void update(Alert updatedAlert) {
        Alert alert  = alertRepository.findByCarAndType(updatedAlert.getCar(), updatedAlert.getType()).get();
        alert.setDescription(updatedAlert.getDescription());
        alert.setTitle(updatedAlert.getTitle());
        alert.setDate(updatedAlert.getDate());
        alertRepository.save(alert);
    }

    public void deleteByCarId(int id){
        alertRepository.findByCar(id).forEach(this::delete);

    }

    public void delete(Alert alert) {
        alertRepository.delete(alert);
    }


    public Alert find(int id) {
        if (alertRepository.findById(id).isPresent())
            return alertRepository.findById(id).get();
        else
            return null;
    }

    public Alert findByTitle(String title) {
        if (alertRepository.findFirstByTitle(title) != null)
            return alertRepository.findFirstByTitle(title);
        else
            return null;
    }

    public List<Alert> findFirstTenAlerts(AlertType type) {
        List<Alert> alerts = alertRepository.findAllByType(type, PageRequest.of(0, 10, Sort.by("date").ascending())).getContent();
        alerts.forEach(alert -> {
            if (alert.getDate().isBefore(LocalDate.now().plusDays(7))) {
                alert.setStyle("danger");
            } else {
                alert.setStyle("dark");
            }
        });
        return alerts;
    }


}
