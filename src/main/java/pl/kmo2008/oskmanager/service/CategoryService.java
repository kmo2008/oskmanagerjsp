package pl.kmo2008.oskmanager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kmo2008.oskmanager.entity.Category;
import pl.kmo2008.oskmanager.repository.CategoryRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.List;

@Service
public class CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    public Category findByName(String name){
        if(categoryRepository.findByName(name).isPresent()){
            return categoryRepository.findByName(name).get();
        }
        else return null;
    }

    public List<Category> findAll(){
        return categoryRepository.findAll();
    }

    public void save(Category category){
        categoryRepository.save(category);
    }

    public HashSet<Category> getCheckedCategories(HttpServletRequest request) {
        List<Category> categories = this.findAll();
        HashSet<Category> checkedCategories = new HashSet<>();
        categories.forEach(cat -> {
            if (request.getParameter(cat.getName()) != null)
                checkedCategories.add(cat);
        });

        return checkedCategories;
    }
}
