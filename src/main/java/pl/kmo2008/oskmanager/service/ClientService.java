package pl.kmo2008.oskmanager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kmo2008.oskmanager.entity.ClientInstructor;
import pl.kmo2008.oskmanager.entity.Course;
import pl.kmo2008.oskmanager.entity.PKK;
import pl.kmo2008.oskmanager.entity.Role;
import pl.kmo2008.oskmanager.entity.enums.Document;
import pl.kmo2008.oskmanager.entity.enums.Sex;
import pl.kmo2008.oskmanager.entity.extensions.Client;
import pl.kmo2008.oskmanager.entity.extensions.Instructor;
import pl.kmo2008.oskmanager.repository.ClientInstructorRepostiory;
import pl.kmo2008.oskmanager.repository.ClientRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Service
@SuppressWarnings("Duplicates")
public class ClientService {

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    @Autowired
    InstructorService instructorService;

    @Autowired
    CourseService courseService;

    @Autowired
    ClientInstructorRepostiory clientInstructorRepostiory;

    public Iterable<Client> findAll() {
        return clientRepository.findAll();
    }

    public Client findById(int id) {
        if (clientRepository.findById(id).isPresent())
            return clientRepository.findById(id).get();
        else return null;
    }

    public void active(Client client) {
        userService.acviteAccount(client);
    }

    public void edit(HttpServletRequest request, int id) {
        Client client = clientRepository.findById(id).get();
        updateFields(request, client);
    }

    public void add(HttpServletRequest request) {
        Client client = new Client();
        updateFields(request, client);
    }

    public void addPkk(Integer id, PKK pkk) {
        Client client = clientRepository.findById(id).get();
        Set<PKK> pkks = client.getPkk();
        pkks.add(pkk);
        client.setPkk(pkks);
        clientRepository.save(client);
    }

    public Client findByPkk(PKK pkk) {
        return clientRepository.findByPkk(pkk).get();
    }

    private void updateFields(HttpServletRequest request, Client client) {
        client.setName(request.getParameter("name"));
        client.setSecondName(request.getParameter("secondName"));
        //client.setPkk(req);
        client.setLastName(request.getParameter("lastName"));
        client.setEmail(request.getParameter("email"));
        String defaultPassword = userService.generateRandomPassword();
        client.setPassword(userService.getbCryptPasswordEncoder().encode(defaultPassword));
        client.setDefaultPassword(defaultPassword);
        client.setPesel(request.getParameter("pesel"));
        if (request.getParameter("sex").equals("WOMAN")) client.setSex(Sex.WOMAN);
        else client.setSex(Sex.MAN);
        client.setStreet(request.getParameter("street"));
        client.setHouseNumber(request.getParameter("houseNumber"));
        client.setApartmentNumber(request.getParameter("apartmentNumber"));
        if (request.getParameter("document").equals("Dowód osobisty")) client.setDocument(Document.ID);
        else if (request.getParameter("document").equals("Paszport")) client.setDocument(Document.PASSPORT);
        else client.setDocument(Document.OTHER);
        client.setDocumentNumber(request.getParameter("documentNumber"));
        client.setPhone(request.getParameter("phone"));
        Role userRole = roleService.find("CLIENT");
        client.setRoles(new HashSet<>(Collections.singletonList(userRole)));
        clientRepository.save(client);
    }

    public Client findClientByEmail(String email) {
        if (clientRepository.findByEmail(email).isPresent())
            return clientRepository.findByEmail(email).get();
        else return null;
    }

    public void addInstructor(int courseId, int clientId, HttpServletRequest request){
        Client client = clientRepository.findById(clientId).get();
        Instructor instructor = instructorService.findById(Integer.parseInt(request.getParameter("instructor")));
        Course course = courseService.findById(courseId);
        ClientInstructor clientInstructor;
        if(clientInstructorRepostiory.findByClientIdAndCourseId(clientId, courseId).isPresent())
            clientInstructor = clientInstructorRepostiory.findByClientIdAndCourseId(clientId, courseId).get();
        else
            clientInstructor = new ClientInstructor();
        clientInstructor.setClient(client);
        clientInstructor.setCourse(course);
        clientInstructor.setInstructor(instructor);
        clientInstructorRepostiory.save(clientInstructor);

    }

    public void delete(int id) {
        clientRepository.delete(clientRepository.findById(id).get());
    }

    public void save(Client client) {
        clientRepository.save(client);
    }
}
