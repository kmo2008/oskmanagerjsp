package pl.kmo2008.oskmanager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kmo2008.oskmanager.entity.Role;
import pl.kmo2008.oskmanager.entity.enums.Document;
import pl.kmo2008.oskmanager.entity.enums.Sex;
import pl.kmo2008.oskmanager.entity.extensions.Instructor;
import pl.kmo2008.oskmanager.repository.InstructorRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@Service
public class InstructorService {

    @Autowired
    InstructorRepository instructorRepository;

    @Autowired
    CategoryService categoryService;

    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    public List<Instructor> findAll() {
        return instructorRepository.findAll();
    }

    public Instructor findById(int id) {
        return instructorRepository.findById(id).get();
    }


    public void add(HttpServletRequest request) {
        Instructor instructor = new Instructor();
        updateFields(request, instructor);
    }

    public void edit(HttpServletRequest request, int id) {
        Instructor instructor = instructorRepository.findById(id).get();
        updateFields(request, instructor);
    }

    public void editName(HttpServletRequest request, int id) {
        Instructor instructor = instructorRepository.findById(id).get();
        instructor.setName(request.getParameter("firstName"));
        instructor.setSecondName(request.getParameter("secondName"));
        instructor.setLastName(request.getParameter("lastName"));
        instructor.setInstructorNumber(request.getParameter("instructorNumber"));
        instructorRepository.save(instructor);
    }

    public void delete(int id) {
        instructorRepository.delete(instructorRepository.findById(id).get());
    }

    public Instructor findInstructorByEmail(String email) {
        return instructorRepository.findByEmail(email);
    }

    private void updateFields(HttpServletRequest request, Instructor instructor) {
        instructor.setName(request.getParameter("name"));
        instructor.setSecondName(request.getParameter("secondName"));
        instructor.setInstructorNumber(request.getParameter("instructorNumber"));
        instructor.setLastName(request.getParameter("lastName"));
        instructor.setEmail(request.getParameter("email"));
        String defaultPassword = userService.generateRandomPassword();
        instructor.setPassword(userService.getbCryptPasswordEncoder().encode(defaultPassword));
        instructor.setDefaultPassword(defaultPassword);
        instructor.setNormalPayment(Double.parseDouble(request.getParameter("normalPayment")));
        instructor.setAdditionalPayment(Double.parseDouble(request.getParameter("additionalPayment")));
        instructor.setCategories(categoryService.getCheckedCategories(request));
        instructor.setPesel(request.getParameter("pesel"));
        if (request.getParameter("sex").equals("WOMAN")) instructor.setSex(Sex.WOMAN);
        else instructor.setSex(Sex.MAN);
        instructor.setStreet(request.getParameter("street"));
        instructor.setHouseNumber(request.getParameter("houseNumber"));
        instructor.setApartmentNumber(request.getParameter("apartmentNumber"));
        if (request.getParameter("document").equals("Dowód osobisty")) instructor.setDocument(Document.ID);
        else if (request.getParameter("document").equals("Paszport")) instructor.setDocument(Document.PASSPORT);
        else instructor.setDocument(Document.OTHER);
        instructor.setDocumentNumber(request.getParameter("documentNumber"));
        instructor.setPhone(request.getParameter("phone"));
        Role userRole = roleService.find("INSTRUCTOR");
        instructor.setRoles(new HashSet<>(Collections.singletonList(userRole)));
        instructorRepository.save(instructor);
    }

    public void active(Instructor instructor) {
        userService.acviteAccount(instructor);
    }

}
