package pl.kmo2008.oskmanager.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kmo2008.oskmanager.entity.Transaction;
import pl.kmo2008.oskmanager.entity.extensions.Client;
import pl.kmo2008.oskmanager.repository.TransactionRepository;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.List;

@Service
public class TransactionService {

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    ClientService clientService;

    @Autowired
    CourseService courseService;

    public List<Transaction> findByClient(Client client) {
        return transactionRepository.findByClient(client);
    }

    public void add(int clientId, HttpServletRequest request){
        Transaction transaction = new Transaction();
        transaction.setClient(clientService.findById(clientId));
        transaction.setCourse(courseService.findById(Integer.parseInt(request.getParameter("courseId"))));
        transaction.setDate(LocalDate.now());
        transaction.setValue(Double.parseDouble(request.getParameter("payment")));
        transactionRepository.save(transaction);
    }

    public void save(Transaction transaction){
        transactionRepository.save(transaction);
    }

    public List<Transaction> findAll(){
        return transactionRepository.findAll();
    }

    public Transaction findById(long id){
        if(transactionRepository.findById(id).isPresent())
            return transactionRepository.findById(id).get();
        else return null;
    }

    public void delete(Transaction transaction){
        transactionRepository.delete(transaction);
    }
}
