package pl.kmo2008.oskmanager.service;

import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.kmo2008.oskmanager.entity.User;
import pl.kmo2008.oskmanager.repository.RoleRepository;
import pl.kmo2008.oskmanager.repository.UserRepository;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@Service
public class UserService {


    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserService(UserRepository userRepository,
                       RoleRepository roleRepository,
                       BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public void saveUser(User user) {
        if(user.getPassword() == null) user.setPassword(generateRandomPassword());
        user.setDefaultPassword(user.getPassword());
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    public int changePassword(HttpServletRequest request){
        if(request.getParameter("password").equals(request.getParameter("password2"))){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            User user = this.findUserByEmail(auth.getName());
            user.setPassword(bCryptPasswordEncoder.encode(request.getParameter("password")));
            return 0;
        }
        else return 1;

    }

    void acviteAccount(User user){
        User nonActiveUser = userRepository.findById(user.getId()).get();
        int active = nonActiveUser.getActive();
        if(active == 1) active = 0;
        else active = 1;
        nonActiveUser.setActive(active);
        nonActiveUser.setPasswordExpiration(LocalDateTime.now());
        userRepository.save(nonActiveUser);
    }


    BCryptPasswordEncoder getbCryptPasswordEncoder() {
        return bCryptPasswordEncoder;
    }

    public String generateRandomPassword() {

        List rules = Arrays.asList(new CharacterRule(EnglishCharacterData.UpperCase, 1),
                new CharacterRule(EnglishCharacterData.LowerCase, 1), new CharacterRule(EnglishCharacterData.Digit, 1));

        PasswordGenerator generator = new PasswordGenerator();
        return generator.generatePassword(8, rules);
    }

}

