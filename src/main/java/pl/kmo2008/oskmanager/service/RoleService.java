package pl.kmo2008.oskmanager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kmo2008.oskmanager.entity.Role;
import pl.kmo2008.oskmanager.repository.RoleRepository;

@Service
public class RoleService {

    @Autowired
    RoleRepository roleRepository;


    public Boolean isRoleWithNameExits(String name){
        if(roleRepository.findByRole(name) != null) return true;
        else return false;
    }

    public void save(Role role){
        roleRepository.save(role);
    }

    public Role find(String name){
        return roleRepository.findByRole(name);
    }

}
