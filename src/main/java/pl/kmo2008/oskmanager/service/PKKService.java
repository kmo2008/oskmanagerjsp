package pl.kmo2008.oskmanager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kmo2008.oskmanager.entity.PKK;
import pl.kmo2008.oskmanager.entity.extensions.Client;
import pl.kmo2008.oskmanager.repository.PKKRepository;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.Set;

@Service
public class PKKService {

    @Autowired
    PKKRepository pkkRepository;

    @Autowired
    CategoryService categoryService;

    @Autowired
    ClientService clientService;

    public void add(Integer clientId, HttpServletRequest request){
        PKK pkk = new PKK();
        pkk.setAddDate(LocalDate.now());
        pkk.setCategories(categoryService.getCheckedCategories(request));
        pkk.setNumber(request.getParameter("pkkNumber"));
        this.save(pkk);
        clientService.addPkk(clientId, pkk);
    }

    public void save(PKK pkk){
        pkkRepository.save(pkk);
    }

    public void delete(Integer id){
        PKK pkk = pkkRepository.findById(id).get();
        Client client = clientService.findByPkk(pkk);
        Set<PKK> pkks = client.getPkk();
        pkks.remove(pkk);
        clientService.save(client);
        pkkRepository.delete(pkk);
    }
}
