package pl.kmo2008.oskmanager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kmo2008.oskmanager.entity.Course;
import pl.kmo2008.oskmanager.entity.extensions.Client;
import pl.kmo2008.oskmanager.repository.CourseRepository;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CourseService {

    @Autowired
    CourseRepository courseRepository;

    @Autowired
    CategoryService categoryService;

    @Autowired
    ClientService clientService;


    public List<Course> findAll() {
        return courseRepository.findAll();
    }

    public Course finByCourseNumber(String courseNumber) {
        if (courseRepository.findByCourseNumber(courseNumber).isPresent())
            return courseRepository.findByCourseNumber(courseNumber).get();
        else return null;
    }

    public void save(Course course) {
        courseRepository.save(course);
    }

    public Course findById(int id) {
        if (courseRepository.findById(id).isPresent())
            return courseRepository.findById(id).get();
        else return null;
    }

    public List<Course> findByClient(Client client) {
        List<Course> clientCourses = this.findAll().stream().filter(course -> course.getClients().contains(client)).collect(Collectors.toList());
        return clientCourses;
    }

    public void addClient(HttpServletRequest request, int id) {
        Course course = courseRepository.findById(id).get();
        List<Client> clients = course.getClients();
        clients.add(clientService.findById(Integer.parseInt(request.getParameter("newClient"))));
        course.setClients(clients);
        courseRepository.save(course);
    }


    public void edit(HttpServletRequest request, int id) {
        Course course = courseRepository.findById(id).get();
        course.setCourseNumber(request.getParameter("courseNumber"));
        course.setPrice(Double.parseDouble(request.getParameter("coursePrice")));
        course.setStartDate(LocalDate.parse(request.getParameter("startDate"), DateTimeFormatter.ISO_LOCAL_DATE));
        if (request.getParameter("endDate").equals(""))
            course.setEndDate(LocalDate.now().plusMonths(4));
        else
            course.setEndDate(LocalDate.parse(request.getParameter("endDate"), DateTimeFormatter.ISO_LOCAL_DATE));
        course.setCategory(categoryService.findByName(request.getParameter("category")));
        courseRepository.save(course);
    }

    public void delete(int id) {
        courseRepository.delete(courseRepository.findById(id).get());
    }
}
