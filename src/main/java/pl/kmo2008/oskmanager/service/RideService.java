package pl.kmo2008.oskmanager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pl.kmo2008.oskmanager.entity.Ride;
import pl.kmo2008.oskmanager.entity.enums.RideType;
import pl.kmo2008.oskmanager.entity.extensions.Instructor;
import pl.kmo2008.oskmanager.repository.RideRepository;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.List;

@Service
public class RideService {

    @Autowired
    RideRepository rideRepository;

    @Autowired
    InstructorService instructorService;

    @Autowired
    ClientService clientService;

    @Autowired
    CarService carService;

    @Autowired
    CourseService courseService;

    public List<Ride> findAll(){
        return rideRepository.findAll();
    }

    public List<Ride> findAllByInstructorId(int id){
        return rideRepository.findAllByInstructorId(id);
    }

    public void save(Ride ride){
        rideRepository.save(ride);
    }

    public void delete(long id){
        rideRepository.delete(rideRepository.findById(id).get());
    }

    public void add(HttpServletRequest request, String date, boolean isInstructor){
        Ride ride = new Ride();
        ride.setDuration(Double.parseDouble(request.getParameter("duration")));
        if(isInstructor){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            Instructor instructor = instructorService.findInstructorByEmail(auth.getName());
            ride.setInstructor(instructor);
        }
        else
        ride.setInstructor(instructorService.findById(Integer.parseInt(request.getParameter("instructorName"))));
        ride.setCourse(courseService.findById(Integer.parseInt(request.getParameter("course"))));
        ride.setClient(clientService.findById(Integer.parseInt(request.getParameter("client"))));
        ride.setCar(carService.findByPlate(request.getParameter("car")));
        if(request.getParameter("type") == "0")
            ride.setRideType(RideType.NORMAL);
        else
            ride.setRideType(RideType.ADDITIONAL);
        ride.setDate(LocalDate.parse(date));
        rideRepository.save(ride);
    }
}
