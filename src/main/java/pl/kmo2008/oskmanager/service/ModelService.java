package pl.kmo2008.oskmanager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;
import pl.kmo2008.oskmanager.entity.User;

@Service
public class ModelService {

    @Autowired
    UserService userService;

    public ModelAndView getNew(){
        ModelAndView modelAndView = new ModelAndView();
        //User default section
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        modelAndView.addObject("user", user);
        return modelAndView;
    }

    public Boolean isClient() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        return user.getRoles().stream().anyMatch(role -> role.getRole().equals("CLIENT"));
    }

    public Boolean isInstrcutor() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        return user.getRoles().stream().anyMatch(role -> role.getRole().equals("INSTRUCTOR"));
    }
}
