package pl.kmo2008.oskmanager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kmo2008.oskmanager.entity.Alert;
import pl.kmo2008.oskmanager.entity.Car;
import pl.kmo2008.oskmanager.entity.enums.AlertType;
import pl.kmo2008.oskmanager.repository.CarRepository;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class CarService {

    @Autowired
    CarRepository carRepository;

    @Autowired
    AlertService alertService;

    @Autowired
    CategoryService categoryService;


    public List<Car> findAll() {
        return carRepository.findAll();
    }

    public Car findByPlate(String plate) {
        if (carRepository.findByPlateEquals(plate).isPresent()) {
            return carRepository.findByPlateEquals(plate).get();
        } else
            return null;
    }

    public void save(Car car) {
        carRepository.save(car);
        alertService.save(new Alert("Badanie techniczne", car.getEndOfReview(), "Koniec badania technicznego: " + car.getPlate() + " (" + car.getMark() + " " + car.getModel() + ")", AlertType.REVIEW, carRepository.findByPlateEquals(car.getPlate()).get().getId()));
        alertService.save(new Alert("Ubezpieczenie", car.getEndOfInsurance(), "Koniec ubezpieczenia: " + car.getPlate() + " (" + car.getMark() + " " + car.getModel() + ")", AlertType.INSURANCE, carRepository.findByPlateEquals(car.getPlate()).get().getId()));
    }

    public void add(HttpServletRequest request) {
        Car car = new Car();
        car.setPlate(request.getParameter("plate"));
        car.setMark(request.getParameter("mark"));
        car.setModel(request.getParameter("model"));
        car.setProductionYear(Integer.parseInt(request.getParameter("productionYear")));
        car.setEndOfInsurance(LocalDate.parse(request.getParameter("insurance"), DateTimeFormatter.ISO_LOCAL_DATE));
        car.setEndOfReview(LocalDate.parse(request.getParameter("review"), DateTimeFormatter.ISO_LOCAL_DATE));
        car.setCategories(categoryService.getCheckedCategories(request));
        this.save(car);
    }

    public void edit(int id, HttpServletRequest request) {
        Car car = carRepository.findById(id).get();
        car.setPlate(request.getParameter("plateEdit"));
        car.setEndOfReview(LocalDate.parse(request.getParameter("reviewEdit"), DateTimeFormatter.ISO_LOCAL_DATE));
        car.setEndOfInsurance(LocalDate.parse(request.getParameter("insuranceEdit"), DateTimeFormatter.ISO_LOCAL_DATE));
        this.save(car);
    }

    public void delete(int id){
        carRepository.deleteById(id);
    }
}
