package pl.kmo2008.oskmanager.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name")
    @OrderColumn
    String name;

    @Override
    public String toString() {
        return "{\"Category\": { \"id\": \""+ id +"\",\"name\": \""+ name +"\"}}";
    }
}
