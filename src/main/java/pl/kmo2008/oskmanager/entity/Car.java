package pl.kmo2008.oskmanager.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Data
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "car_id")
    int id;

    @Column(name = "mark")
    String mark;

    @Column(name = "model")
    String model;

    @Column(name = "plate")
    String plate;

    @Column(name = "productionYear")
    Integer productionYear;

    @Column(name = "endOfReview")
    LocalDate endOfReview;

    @Column(name = "endOfInsurance")
    LocalDate endOfInsurance;

    @ManyToMany
    @JoinTable(name = "car_categories", joinColumns = @JoinColumn(name = "car_id"), inverseJoinColumns = @JoinColumn(name = "category_id"))
    @OrderBy("name")
    private Set<Category> categories;
}
