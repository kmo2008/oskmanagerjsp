package pl.kmo2008.oskmanager.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Data
public class PKK {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    @Column(name = "number")
    String number;

    @ManyToMany
    @JoinTable(name = "pkk_categories", joinColumns = @JoinColumn(name = "pkk_id"), inverseJoinColumns = @JoinColumn(name = "category_id"))
    @OrderBy("name")
    private Set<Category> categories;

    @Column(name = "addDate")
    LocalDate addDate;
}
