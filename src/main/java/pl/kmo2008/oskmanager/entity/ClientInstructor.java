package pl.kmo2008.oskmanager.entity;

import lombok.Data;
import pl.kmo2008.oskmanager.entity.extensions.Client;
import pl.kmo2008.oskmanager.entity.extensions.Instructor;

import javax.persistence.*;

@Entity
@Data
public class ClientInstructor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "course_id")
    Course course;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "instructor_id")
    Instructor instructor;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id")
    Client client;
}
