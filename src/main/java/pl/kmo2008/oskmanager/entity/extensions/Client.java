package pl.kmo2008.oskmanager.entity.extensions;

import lombok.Data;
import lombok.EqualsAndHashCode;
import pl.kmo2008.oskmanager.entity.PKK;
import pl.kmo2008.oskmanager.entity.User;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class Client extends User {

    @ManyToMany
    @JoinTable(name = "client_pkk", joinColumns = @JoinColumn(name = "cleint_id"), inverseJoinColumns = @JoinColumn(name = "pkk_id"))
    private Set<PKK> pkk;

}
