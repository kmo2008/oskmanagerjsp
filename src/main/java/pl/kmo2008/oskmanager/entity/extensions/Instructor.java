package pl.kmo2008.oskmanager.entity.extensions;

import lombok.Data;
import lombok.EqualsAndHashCode;
import pl.kmo2008.oskmanager.entity.Car;
import pl.kmo2008.oskmanager.entity.Category;
import pl.kmo2008.oskmanager.entity.User;

import javax.persistence.*;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class Instructor extends User {

    @ManyToMany
    @JoinTable(name = "instructor_categories", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "category_id"))
    @OrderBy("name")
    private Set<Category> categories;

    @Column(name = "normalPayment")
    private double normalPayment;

    @Column(name = "additionalPayment")
    private double additionalPayment;

    @Column(name = "intructorNumber")
    private String instructorNumber;

    @ManyToMany
    @JoinTable(name = "instructor_cars", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "car_id"))
    @OrderBy("plate")
    private Set<Car> cars;
}
