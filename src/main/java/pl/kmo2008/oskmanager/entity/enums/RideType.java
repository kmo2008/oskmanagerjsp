package pl.kmo2008.oskmanager.entity.enums;

public enum RideType {
    NORMAL, ADDITIONAL
}
