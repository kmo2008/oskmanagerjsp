package pl.kmo2008.oskmanager.entity.enums;

public enum Sex {
    MAN, WOMAN, NONE
}
