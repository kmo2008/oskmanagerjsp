package pl.kmo2008.oskmanager.entity.enums;

public enum AlertType {
    REVIEW, INSURANCE
}
