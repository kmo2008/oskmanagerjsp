package pl.kmo2008.oskmanager.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import pl.kmo2008.oskmanager.entity.enums.AlertType;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "alerts")
@NoArgsConstructor
public class Alert {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "alert_id")
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "description")
    private String description;

    @Column(name = "type")
    private AlertType type;

    @Column(name = "car_id")
    private Integer car;

    @Transient
    private String style;

    public Alert(String title, LocalDate date, String description, AlertType type, Integer car) {
        this.title = title;
        this.date = date;
        this.description = description;
        this.type = type;
        this.car = car;
    }
}
