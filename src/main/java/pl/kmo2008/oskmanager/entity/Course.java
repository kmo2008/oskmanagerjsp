package pl.kmo2008.oskmanager.entity;

import lombok.Data;
import pl.kmo2008.oskmanager.entity.extensions.Client;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;

    @Column(name = "courseNumber")
    String courseNumber;

    @Column(name = "startDate")
    LocalDate startDate;

    @Column(name = "endDate")
    LocalDate endDate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id")
    Category category;

    @Column(name = "price")
    double price;

    @ManyToMany
    @JoinTable(name = "curse_client", joinColumns = @JoinColumn(name = "curse_id"), inverseJoinColumns = @JoinColumn(name = "client_id"))
    private List<Client> clients;
}
