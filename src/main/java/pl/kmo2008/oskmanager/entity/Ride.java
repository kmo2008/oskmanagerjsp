package pl.kmo2008.oskmanager.entity;

import lombok.Data;
import pl.kmo2008.oskmanager.entity.enums.RideType;
import pl.kmo2008.oskmanager.entity.extensions.Client;
import pl.kmo2008.oskmanager.entity.extensions.Instructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
public class Ride {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "date")
    LocalDate date;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "course_id")
    Course course;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id")
    Client client;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "instructor_id")
    Instructor instructor;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "car_id")
    Car car;

    @Column(name = "duration")
    Double duration;

    @Column(name = "type")
    RideType rideType;


    public int getMotnh(){
        return this.date.getMonthValue();
    }
}
