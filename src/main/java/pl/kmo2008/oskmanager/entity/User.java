package pl.kmo2008.oskmanager.entity;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import pl.kmo2008.oskmanager.entity.enums.Document;
import pl.kmo2008.oskmanager.entity.enums.Sex;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.Set;

@Data
@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private int id;

    @Column(name = "email")
    @Email(message = "*Please provide a valid Email")
    @NotEmpty(message = "*Please provide an email")
    private String email;

    @Column(name = "password")
    @Length(min = 5, message = "*Your password must have at least 5 characters")
    @NotEmpty(message = "*Please provide your password")
    private String password;

    @Column(name = "name")
    @NotEmpty(message = "*Please provide your name")
    private String name;

    @Column(name = "secondName")
    private String secondName;

    @Column(name = "last_name")
    @NotEmpty(message = "*Please provide your last name")
    private String lastName;

    @Column(name = "active")
    private int active;

    @ManyToMany
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    @Column(name = "sex")
    private Sex sex;

    @Column(name = "street")
    private String street;

    @Column(name = "houseNumber")
    private String houseNumber;

    @Column(name = "apartmentNumber")
    private String apartmentNumber;

    @Column(name = "postCode")
    private String postCode;

    @Column(name = "City")
    private String city;

    @Column(name = "pesel")
    private String pesel;

    @Column(name = "document")
    private Document document;

    @Column(name = "documentNumber")
    private String documentNumber;

    @Column(name = "phone")
    private String phone;

    @Column(name = "expirationDate")
    private LocalDateTime expirationDate;

    @Column(name = "passwordExpiration")
    private LocalDateTime passwordExpiration;

    @Column(name = "defaultPassword")
    private String defaultPassword;


    public String getFullName() {
        if (this.secondName != null)
            return this.name + " " + this.secondName + " " + this.lastName;
        else
            return this.name + " " + this.lastName;
    }

}
