package pl.kmo2008.oskmanager.entity;

import lombok.Data;
import pl.kmo2008.oskmanager.entity.extensions.Client;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "course_id")
    Course course;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id")
    Client client;

    @Column(name = "date")
    LocalDate date;

    @Column(name = "value")
    Double value;
}
